---
date: 2019-01-01
identifier: vision
layout: layouts/page.njk
permalink: /{{ locale }}/{{ parent | slug }}/{{ navtitle | slug }}.html
navtitle: Vision und Mission
---
<div class="flex wrap justify-center">
<div class="item pa-r">

![](/static/mission.jpg)
# Mission
Ein würdevolles Leben und die Autonomie der indigenen, afroabstammenden und bäuerlichen Gemeinschaften durch die Stärkung der Bildung und die Förderung von Projekten zu gewährleisten, wobei die CHC als Brücke dienen soll. Ebenso die Suche nach finanziellen Mitteln mit dem Ziel, wirtschaftliche, ökologische und kulturelle Lösungen zu erzielen, für jene Projekte, die Priorität für die Gemeinschaften haben.
</div>
<div class="item ">

![](/static/vision.jpg)
# Vision
Wir wollen soziale und produktive Projekte entwickeln, die es erlauben, Autonomie zu erlangen und die Lebensqualität ethnischer und ländlicher Minderheiten durch erfahrungsbezogene Begegnungen, Forschung,  künstlerisches Schaffen sowie durch umweltbezogene, kulturelle und traditionelle Initiativen zu verbessern. Rituale, die die Transformation von imaginären und sozialen Beziehungen ermöglichen.
</div>
</div>
