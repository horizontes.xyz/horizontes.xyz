---
identifier: arley
title: Arley Buitrago Landázuri
img: Miembros/Arley.jpg
---

Afro-Nachkomme, geboren 1985 in Bogotá. Er hat einen Abschluss in Sozialwissenschaften und einen Master in Kunstwissenschaften mit Erfahrung und Berufung zur Gemeinschaftsarbeit. In den letzten  Jahren hat er  mit der Erziehungseinrichtung “Arte Social Videos y Rollos”, dem Kollektiv “Entre-tránsitos”, der Nationalen Bewegung “Cimarron” y der “Corporación Horizontes Colombianos -CHC-“ (jetzt assoziiertes Mitglied) zusammengearbeitet. Im ständigen Interesse an der Artikulation kultureller und gemeinschaftlicher und künstlerischer Prozesse hat er künstlerische Praktiken wie Performance- und Videokunst mit einem geschlechtsspezifischen, interkulturellen und interdisziplinären Ansatz entwickelt. Er hat die Artikel "Erinnerungen an eine ununterbrochene Qual", "Die magische religiöse Praxis des Candomblé in Bogotá" und "Das Schisma, einen schwarzen Körper zu bewohnen" veröffentlicht.
