---
identifier: doro
title: Dorothea Wolf – Nuernberg
img: Miembros/Doro.jpg
---

Sie wurde am 9. Januar 1962 in Deutschland geboren und war 15 Jahre lang als professionelle Krankenschwester tätig. Sie arbeitete mehrere Jahre in gemeinnützigen Organisationen in verschiedenen Sektoren ihres Landes und gründete eine NGO, die bis heute Obdachlosen dient. Zwei Jahre lang arbeitete sie in Verwaltungstätigkeiten der israelischen Botschaft in Deutschland. Sie lebte und arbeitete ein Jahr in Ginosar / Galiläa / Israel (soziale Freiwilligentätigkeit). Seit 1999 lebt sie in Bogotá / Kolumbien, wo sie zunächst Privatunterricht in Deutsch und Englisch gab (1999 - 2000) und anschließend im audiovisuellen Medienbereich experimentieren wollte (2000 - 2002). Im Oktober 2002 beschloss sie, die gemeinnützige Organisation Corporación Horizontes Colombianos -CHC- zu gründen, deren gesetzliche Vertreterin und Geschäftsführerin ist. Sie hat 37 Jahre Erfahrung als freiwillige Helferin und in der Koordination von Entwicklungshelfern.
Sprachen: Deutsch, Spanisch, Englisch, Hebräisch und Französisch.

Sie ist ein Mensch mit einem hohen sozialen Verantwortungsbewusstsein und Engagement für die Erzielung von Ergebnissen. Anspruch auf persönlichen Wachstum mit großen Stärken, um in einem Team zu arbeiten, und immer mit der Erwartungen an dauerhaftem Fortschritt.
