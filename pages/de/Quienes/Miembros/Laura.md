---
identifier: laura
title: Laura Cristina Bahamón Villalba
img: Miembros/Laura.jpg
---

Geboren am 9. März 1989 in Neiva-Huila (Kolumbien). Abschluss in Kunsterziehung und Kultur sowie Spezialisierung in Kindheit und Jugend. Verbunden mit der pädagogischen Arbeit mit Kindern und Jugendlichen der Nasa-Indianergemeinden in den Departements Huila und Cauca hat sie auch Bildungs- und Körperkunstpraktiken für Kinder mit sonderpädagogischem Förderbedarf entwickelt. Grosses Interesse an der Entwicklung künstlerischer und kultureller Praktiken für eine soziale Transformation. Mitglied und freiwillige Helferin der Corporación Horizontes Colombianos
-CHC- seit 2018.
