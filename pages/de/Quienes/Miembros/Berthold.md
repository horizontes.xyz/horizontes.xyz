---
identifier: berthold
title: Berthold Wolff
img: Miembros/Berthold.jpg
---


Geboren am 02. März 1965 in Hilden. 1984 Abitur am Helmholtz-Gymnasium Hilden. 1989 Studium der Theologie in Bonn und Wien; Diplomabschluss
1992 Priesterweihe in Köln; danach zwei Kaplanszeiten von je 4 Jahren in Brühl und in Bensberg.

Seit 2000 Pfarrer und Schulseelsorger am Erzbischöflichen St. Ursula Gymnasium und an der Elisabeth von Thüringen Realschule in Brühl. Im September 2009 zum Gemeindepfarrer ernannt in der Pfarrei St. Maximilian Kolbe in Köln Porz und 10 Jahre später zum leitenden Pfarrer des ‘Sendungsraums Porz’.  Seit 1993 Gründungsmitglied und Vorsitzender des gemeinnützigen Vereins zur Ausbildungsförderung Jugendlicher in der Dominikanischen Republik (AJD e.V.) Im Rahmen dieser Tätigkeit reist er seit 2000 alle drei Jahre mit einer Gruppe von Interessierten in die Dominikanische Republik.
