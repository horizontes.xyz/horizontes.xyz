---
identifier: narcisa
title: Narcisa Bautista Ramos
img: Miembros/Narcisa.jpg
---

Gehört der Ethnie Tikuna an.
Zweisprachige Studentin im Bereich der demokratischen, politischen, indigenen Bildung in der Stiftung „Caminos de la Identidad“ – FUCAI. Führungspersönlichkeit ihres Stammes Nazareth im kolumbianischen Amazonas – Trapez. Sie zeichnete sich als Vizegoberneurin aus und leitet heute Projekte mit Kindern, Jugendlichen und Frauen ihres Stammes und anderen Stämmen der Region.
Sie absolvierte Kurse im Bereich der traditionellen Küche des Amazonas, im Genossenschaftsbereich und Sportbereich.
Sie ist Mitglied der Ureinwohnervereinigung der Autoritäten des Amazonas – Trapez (ACITAM) und aktives Mitglied der -CHC- seit 2008, spezialisiert in der Arbeit mit indigene Frauen.
