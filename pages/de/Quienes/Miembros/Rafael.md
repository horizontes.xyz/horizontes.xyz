---
identifier: rafael
title: Rafael Antonio Chaparro Torres
img: Miembros/Rafa.jpg
---

Er ist 33 Jahre alt.
Region: Tota, Boyacá, Kolumbien
Umweltbeauftragter mit einem Master-Abschluss in Geografischen Informationssystemen.
Nachkomme von Großeltern indigener Bauern, die sich für traditionelle Gemeinschaften und das althergebrachte Wissen lateinamerikanischer Vorfahren interessieren. Seit 2009 nimmt er an verschiedenen Initiativen und Programmen der sozialen Gemeinschaft teil, die sich mit dem Denken der Vorfahren, der Umwelterziehung und der Anerkennung von Gebieten (Maya-Route, Inka-Route) befassen. Bis 2014 Direktor des Pachakutia-Kollektivs, einer Jugendorganisation, die sich um die Umwelterziehung aus den Werten ihrer Vorfahren und der Kunst der Gemeinschaft bemüht.
Freiwilliges Mitglied der Corporación Horizontes Colombianos -CHC- seit 14 Jahren, Mitarbeiter im Projektmanagement sowie Spezialist in Forschungs- und Umweltfragen. Seit Juni 2019 assoziiertes Mitglied und stellvertretender Geschäftsführer der CHC.
