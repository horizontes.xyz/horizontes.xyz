---
identifier: caroline
title: Caroline Schiller
img: Miembros/Caroline.jpg
---

Lehrerin einer Freiwilligentätigkeit an der Waldorfschule in Bogotá (Inti Huasi, „Casa del Sol“), unterrichtet Englisch, Deutsch und arbeitet in der Selbsthilfegruppe mit behinderten Kindern zusammen.
Doppelabschluss in Betriebswirtschaft und Volkswirtschaft (University of Glasgow, Schottland).

Sie wurde am 20. Mai 1995 in München geboren, ihre Eltern stammen beide aus der Tschechischen Republik. Nach vier Jahren Studium der Wirtschaftswissenschaften und mehreren Tätigkeiten in diesem Bereich (einschließlich der Position bei Deloitte Consultancy in Leeds und der Position im Vertrieb in Zionsville, Indiana, bei Southwestern Advantage) entschied sie sich, ihre Herangehensweise an Projekte zu ändern, die nichts mit Finanzen, Vertrieb, Handel und Gewerbe zu tun haben.

Die Freiwilligenarbeit auf der Ökofarm Can Decreix (Cerbere, Frankreich) gab ihr die Gelegenheit, wirtschaftliche Alternativen zu entdecken, d.h. "Degrowth" und Bewegungen wie Slow Food und Food Sharing, um das Problem der Lebensmittelverschwendung und des Lebensmittelverlusts lokaler Lebensmittel zu bekämpfen. Zu den weiteren Freiwilligenstandorten in Europa zählen die Eco Krishna Farm in Lesmahagow (Schottland), Mirapuri (in Mirapuri-Coiromonte, Italien) und die Menschliche Welt (Yoga Ashram in Deutschland bei Ravensburg), die ihr jeweils geholfen haben die Notwendigkeit zu unterstreichen, für das Leben zu arbeiten, im Einklang mit der Gemeinschaft und im Zusammenleben mit der Natur.

Gegenwärtig wurde an der Waldorfschule "Inti Huasi" (Casa del Sol) ihre Vision erweitert und auf Bildung fokussiert, was auch ihr Ziel und ihre Leidenschaft in der Corporación Horizontes Colombianos
-CHC- in Kolumbien ist, der sie sich 2018 als aktives Mitglied anschloss.
