---
identifier: juvencio
title: Juvencio Manduca
img: Miembros/Juvencio.jpg
---

Er gehört der Ethnie Tikuna an.

Geboren am 16. Maerz 1981 im Reservat Nazareth / Amazonas.
Er besuchte die Grundschule Nazareth’s und beendete seine schulische Laufbahn mit dem Abitur an der Ureinwohnerschule San Juan Bosco in Leticia / Amazonas.

Von März – Mai 2007 absolviert er ein akademisches Angleichungssemester an der Nationalen Universitaet Kolumbiens – Leticia und nahm ebenfalls an einem Universitätsvorbereitungskurs im September 2007, teil Er lebt derzeit mit seinen Eltern Pedro Pereira Ramos und Ligia Manduca Guillermo, sowie mit seiner Ehefrau Luz Emerita Moran Ahue und seinem Sohn Camilo im Reservat von Nazareth.

Die Familie lebt vom Ackerbau und der Fischerei. Juvencio Pereira Manduca, konnte lediglich die Schule abschliessen; ein Hochschulstudium blieb ihm aus finanziellen Gründen versagt. Seit er die Schule verlassen hatte, stellte er sein Wissen dem Stamm zur Verfügung, indem er als Anführer im Stamm, gemeinsam mit den obersten Autoritäten, zusammenarbeitete.

Im März 2006 schrieb er sich ins Hochschulbildungsprogramm der Corporación Horizontes Colombianos -CHC- ein und mit einer kontinuierlichen akademischen Anpassung, begann er 2008 mit seinem Jurastudium, um als zukünftiger Anwalt, die Rechte seines Volkes zu verteidigen.

Seit Januar 2006 gehört er als Ehrenmitglied der Corporación Horizontes Colombianos an; seit Januar 2008 Ratsmitglied und Sekretär der Generalversammlung der -CHC-.
