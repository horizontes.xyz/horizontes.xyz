---
date: 2019-01-02
identifier: objetivos
permalink: /{{ locale }}/{{ parent | slug }}/ziele.html
title: Zielsetzung
img: objectives.jpg
---

Unsere Arbeit befindet sich im Einvernehmen mit den Objektiven der Jahrtausendwende. Unter ihnen die Ausmerzung der Armut, das Eröffnen von Bildungschancen, die Verminderung der Kindersterblichkeit, die Förderung der Emanzipation und des Respekts vor der indigenen Kultur und deren Traditionen.

Mittels unserer Projekte streben wir folgende Ziele an:
 - Die Schaffung von mehr und bessere Möglichkeiten für die indigene Bevölkerung.
 - Förderung und Vereinfachung des Zugriffs auf Hochschulbildung ohne geschlechtsspezifische Unterscheidung.
 - Schaffung friedlicher Verteidigungsmechanismen für Stämme, die sich inmitten des bewaffneten Konflikts befinden.
 - Größere Autonomie und Inklusion schaffen.
 - Stärkung des indigenen Stolzes.
 - Verbesserung der Lebensumstände für die Kinder in den Stämmen.
 - Das Schaffen einer Atmosphäre des Positivismus und der Motivation.
 - Erhöhung des Einkommens und damit des Lebensstandards.
 - Das Stärken von Traditionen, Werten und der ursprünglichen Identität.
 - Verbesserung der Gesundheits- und Hygienebedingungen der Bevölkerung.
 - Das Schaffen eines gesünderen und produktiveren Umfelds.
 - Die Verbesserung der Bedingungen und der Stabilität in den Stämmen.
 - Das Erzielen von neuem und fortgeschrittenerem Wissen durch die Nutzung der neuen Technologien.
