---
date: 2019-01-02
identifier: quienes
layout: layouts/page.njk
permalink: /{{ locale }}/{{ parent | slug }}/über-uns.html
title: Über uns
tags:
 - nav_de
---
<center>

![](/static/Logo_CHC.png)
</center>

Die Corporación Horizontes Colombianos – CHC ist eine gemeinnützige Organisation, die für die Entwicklung der indigenen Völker, der afro-abstammenden und bäuerlichen Bevölkerung arbeitet und überzeugt von  der lebenswichtigen Bedeutung ist, was dies für die Erhaltung des natürlichen, kulturellen und ethnischen Erbe mit sich bringt.

Mit dem Ziel, die Autonomie und die Einbeziehung dieser Völker zu stärken und dessen Überleben zu sichern, bieten wir den Gemeinschaften Mittel, damit sie ihre Rechte geltend machen und Architekten ihrer eigenen Entwicklung werden. Auf diese Weise tragen sie mit ihrem Wissen, ihrer Erfahrung und ihrer Weisheit zur Gesellschaft bei, welche im modernen Leben eine immer wichtigere Rolle einnehmen, schattiert durch den Missbrauch natürlicher Ressourcen, Umweltkatastrophen und die Denaturierung von Werten.
