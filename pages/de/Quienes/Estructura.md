---
date: 2019-01-05
identifier: estructura
permalink: /{{ locale }}/{{ parent | slug }}/{{ title | slug }}.html
title: Statuten
---
**SATZUNG CORPORACIÓN HORIZONTES COLOMBIANOS**

**KAPITEL I: NAME, SITZ, SOZIALE ZIELSETZUNG UND EXISTENZDAUER DER EINRICHTUNG**

**ARTIKEL 1: NAME DER EINRICHTUNG**
Die Einrichtung, die sich den Regeln dieser Satzung unterwirft, ist eine gemeinnützige Einrichtung; sie wird als Körperschaft gegründet und namentlich als CORPORACIÓN HORIZONTES COLOMBIANOS aufgeführt. Sie ist berechtigt auch unter dem Zeichen C.H.C. zu handeln.

**ARTIKEL 2: SITZ**
Die Einrichtung hat ihren Sitz in Bogotá D.C. / Kolumbien, Südamerika und ihr Hauptbüro in der CR 6 B Este No 88 G – 18 Sur, Telefon: 310 254 5321 – 312 604 9037; sie ist berechtigt, Büros ausserhalb Bogotás und in anderen Ländern der Welt zu öffnen.


**ARTIKEL 3: DAUER**
Die Einrichtung, die gegründet wird, hat vom 17. Oktober 2002 an gerechnet eine Existenzdauer von sechzig (60) Jahren

**ARTIKEL 4: SOZIALE ZIELSETZUNG**
Das Erlangen finanzieller und institutioneller Unterstützung seitens privat und juristischer Personen, mit dem Ziel, die Bildungssituation, soziales Wohlergehen sowie die Sichtbarmachung, Förderung und Verbreitung der Praktiken, Kenntnisse und Traditionen der indigenen, afro- und bäuerlichen Gemeinschaften und ihrer vielfältigen Erscheinungsformen, verbessern.

Die Förderung von Initiativen, Programmen oder Projekten der ganzheitlichen traditionellen Medizin mit interkulturellem Ansatz, Umwelterziehung, Gemeinschaftsforschung, Forschungsschaffung, alternativem Tourismus, Koexistenz und Friedenserziehung, begleitet von Aktivitäten zur Verbesserung der Lebensqualität und des Miteinanderlebens.

Die Förderung von Aktionen für Frieden, Koexistenz, Umwelt, traditionelles Wissen und Menschenrechte sowie Förderung von Kunst und Ritualen als Strategien der sozialen Bewusstseinsbildung unter den Gemeinschaften mit interkulturellem und erfahrungsorientiertem Ansatz.

Das Ermöglichen des kulturellen Miteinanders mit Menschen aus verschiedenen Ländern, die Interesse am Austausch mit den angestammten und ländlichen Gemeinden in Kolumbien und Lateinamerika haben.

[Vollständiges Dokument als pdf herunterladen](/static/estatutos_pdf_de_ak.pdf)
