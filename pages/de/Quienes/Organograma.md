---
date: 2019-01-04
identifier: organograma
permalink: /{{ locale }}/{{ parent | slug }}/organisationsplan.html
title: Organisationsplan
layout: layouts/chart.njk
---

## Generalversammlung

<pre class="mermaid">
  graph TD

  A[Generalversammlung] --- B1[Finanzprüfer]
  A --- B2[Mitgliederrat]
  B2 --- C1[Gesetzlicher Vertreter]
  C1 --- D1[Beratungsgruppe und <br/> Vereinbarungen]
  C1 --- D2[Indigene Vertretung]
  D2 --- E1[Repräsentant in <br/> Deutschland]
  E1 --- F1[Vertreter der <br/> Geschäftsführung]
  B2 --- C2[Interkulturelle <br/> Kommunikatorin]
  C2 --- D3[Kulturmagerin]
  D3 --- E2[Projektberater]
  E2 --- F2[Umweltbereichskoordinator]
  F2 --- G2[Rechtsberater]

  classDef someclass fill:#f96;
  class A someclass;
</pre>

## Beratungsgruppe und Vereinbarungen

<pre class="mermaid">
  graph LR
  A[Beratungsgruppe und <br/> Vereinbarungen]
  A --- B1[Pontificia Universiad <br/> Javeriana]
  B1 --- C1[Fak. für Bauingenieurwesen]
  B1 --- C2[Fak. für Buchhaltung]
  B1 --- C3[Institut für Betriebswirtschaftslehre]
  B1 --- C4[Fak. für Umwelt und Ländliche Studien]
  B1 --- C5[Technische Fak.]
  B1 --- C6[Fak. für Psychologie]  
  B1 --- C7[Fak. für moderne Sprachen]  
  B1 --- C8[Krankenpflegeschule]
  B1 --- C9[Juristische Fak.]
  A --- B2[Politécnico Grancolombiano]
  B2 --- C10[Fak. für Audiovisuelle Medien]
  B2 --- C11[Juristische Fak.]
  A --- B3[Andere Universitäten]
  B3 --- C12[Univ. Jorge Tadeo Lozano]
  B3 --- C13[Univ. Nacional de Colombia Sitz - Amazonas]
  B3 --- C14[Univ. Pedagógica Bogotá]
  B3 --- C15[Univ. Nacional de Colombia Sitz - Medellin]
  B3 --- C16[Univ. Nacional Sitz - Palmira]
  A --- B4[Freiwilligen- Netzwerke]
  B4 --- C17[Alianza Social Uniandinos]
  B4 --- C18[ONU Voluntariado]
  B4 --- C19[FONDACIO]
  A --- B5[Horizontes e. V- Alemania]
  A --- B6[ACITAM]
  B6 --- C20[Asociación de Cabildos Indigenas del Trapecio Amazónico]
  A --- B7[ONIC]
  B7 --- C21[Organización Nacional Indigena de Colombia]
  A --- B8[Andere Allianzen]
  B8 --- C22[Parroquia Maximilian Kolbe]
  B8 --- C23[EVT - Alemania]
  B8 --- C24[Tu Patrocinnio]
  B8 --- C25[Aid Connect]
  B8 --- C26[We care Charity Label]
  B8 --- C27[Parroquia Frohnleichnam]

  classDef someclass fill:#f96;
  class A someclass;
  class B1 someclass;
  class B2 someclass;
  class B3 someclass;
  class B4 someclass;
  class B5 someclass;
  class B6 someclass;
  class B7 someclass;
  class B8 someclass;
</pre>
