---
identifier: Iburi
layout: layouts/gallery.njk
permalink: /{{ locale }}/{{ parent | slug }}/iburi.html
title: IBURI - unsere indigene Sichtweise
img: IBURI_foto_1.jpg
pics: 14
---

IBURI war ein geschlossener Fernsehsender mit Programmen, die sich speziell auf die Gemeinschaft Nazareth des Amazonas und die umliegenden Tikuna - Stämme konzentrierte. Ziel war es, durch audiovisuelle Präsentationen, in denen wichtige Themen über ihre Kultur aufgezeigt wurden, die Tikuna-Kultur zu stärken und wo gleichzeitig alte Praktiken als audiovisuelles Gedächtnis für zukünftige Generationen erhalten bleiben sollten.

Die monatliche Sendung wurde in Tikuna und Spanisch ausgestrahlt, wo eine Nachrichtensendung mit hohem regionalem Inhalt, Dokumentarfilme, Kurzfilme, Videoclips, Spielfilme, Kampagnen zur Bildung und zur Wiederherstellung von Werten gezeigt wurden. Alle Inhalte waren das Ergebnis der Zusammenarbeit mit dem Stamm. Audiovisuelles Equipment wurde dem Stamm übergeben und die Direktorin des Kanals Luz Dary Mojica wurde für seinen zukünftigen Betrieb ernannt, wo die indigene Bevölkerung ihre eigenen Programme drehen wird und deren Aussendung innerhalb der Gemeinschaft und der anderen Tikunas - Stämme des Amazonas gewährleistet sein wird.

Im Rahmen des Projekts entstand der Film "El Origen del Pueblo Tikuna".
IBURI war ein Projekt der CHC mit Unterstützung der Universität Politécnico Grancolombiano / Bogotá.
