---
identifier: Sanitation
layout: layouts/gallery.njk
permalink: /{{ locale }}/{{ parent | slug }}/grundlegende-hygiene.html
title: Grundlegende Hygiene
img: Sanitation/12.jpg
pics: 14
---

Das „Pilotprojekt zur Umsetzung grundlegender Hygienemethoden und -strategien in indigenen Stämmen Kolumbiens, Fall: Tikuna - Stamm Nazareth/ Amazonas“, war eine Initiative der CHC in Zusammenarbeit mit der Faultät für Bauingenieurwesen der Pontificia Universidad Javeriana Bogotás, das sich bemühte, die sanitären Bedingungen der Stämme zu verbessern, indem die Versorgung mit Trinkwasser und Abwasser verbessert und eine ordnungsgemäße Entsorgung fester Abfälle sichergestellt werden sollte.

Die Studien und Entwürfe wurden von der Vereinigung der indigenen Cabildos des Amazonas-Trapezes (ACITAM), dem Umweltministerium, Corpoamazonía, der Regierung des Amazonas und dem Bürgermeisteramt von Leticia dokumentiert und gebilligt. Der Stamm machte sich für die Geldbeschaffung in Spanien zur Realisierung dieses Projektes verantwortlich.
