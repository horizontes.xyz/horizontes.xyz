---
identifier: DomingosCulturales
layout: layouts/gallery.njk
permalink: /{{ locale }}/{{ parent | slug }}/{{ title | slug }}.html
title: Kultursonntage
img: DomingosCulturales.jpg
pics: 3
---

Das Projekt kann als eines der ersten bezeichnet werden, welches die CHC eingeführt hat, mit dem Ziel Kunst den  populären Klassen näher zu bringen. Es bot sich ihnen die Möglichkeit, in einem didaktischen Rahmen und in direkter Kommunikation mit einigen Künstlern deren Kunstwerke zu betrachten, zu analysieren, zu diskutieren und selbst Kunst zu schaffen.

Wir haben durch diesen Kontakt mit der Kunst eine Transformation der Menschen angestrebt, um ein Gleichgewicht in jedem einzelnen zu erzielen, ein besseres Verständnis für die Welt und das Leben zu erlangen und sie zu einer positiven Einstellung zu führen.

Wir konzentrierten unsere Arbeit auf „Consuelo Sur“, ein bekanntes Viertel im südlichen Teil Bogotás, in Zusammenarbeit mit der “Junta de Acción Comunal” (Aktionsausschuss),  um die Orte der Veranstaltungen festzulegen und die Art und Weise, wie wir die Bewohner einberiefen, inmitten eines Angebotes künstlerischer Praktiken wie (Musik, Theater, zeitgenössischer Tanz, Literatur, Poesie und Dokumentarfilme zu verschiedenen Themen).

Die CHC erhielt Spenden in Form von Snacks, insbesondere für Kinder, ebenso kleine nützliche Geschenke, die an die Teilnehmer verteilt wurden.

Wir hatten während unserer Besuche ein weiteres Projekt mit einbezogen, das wir als „Müll-Nicht gleich Müll“ bezeichneten und in dem wir die Menschen dazu ermutigten, sogenannte Abfälle wiederzuverwenden und zu recyceln, ebenso um in Zusammenarbeit mit Künstlern,  die Erfahrungen des künstlerischen Schaffens mit dem vermeintlichen Müll zu machen.

Auf der Ladefläche eines Lastwagens und einem Megafon fuhren wir durch die Straßen, um die Bevölkerung zu versammeln und auf dem Fußballfeld des Viertels fanden wir den geeigneten Arbeitsplatz mit ausgezeichneter Einwohnerbeteiligung, was uns erlaubte, uns mit der Gemeinschaft auszutauschen und Räume für ein friedliches und künstlerisches Zusammenleben zu schaffen.
