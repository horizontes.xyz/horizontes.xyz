---
identifier: Artesanos
layout: layouts/gallery.njk
permalink:  /{{ locale }}/{{ parent | slug }}/kunsthandwerker.html
title: Aufbau einer Genossenschaft für Kunsthandwerker des Amazonas - Trapez
img: artesanos_foto_1.jpg
pics: 12
---

Mit diesem Projekt beabsichtigten wir, eine umfassende soziale Entwicklung der Ursprungsvölker des Amazonas-Trapezes zu erzielen, produktive Räume zu schaffen und die Fähigkeiten des Zusammenschliessens und des Genossenschaftswesen unter den Kunsthandwerkern zu fördern, um auf diese Weise die Arbeit ihrer Vorfahren wieder auflebenzulassen und in der Region allgemein anzuregen.

Gleichzeitig wurden Kanäle für den Vertrieb und den Verkauf von Produkten im Rahmen von Allianzen und eigenen Verkaufspunkten im Rahmen des Konzepts des fairen Handels eröffnet, um das Einkommen und die Lebensbedingungen von 1.500 Kunstgewerbetreibenden in den 18 Stämmen zu verbessern und sie mit dem Projekt zu verbinden.

Diese Initiative wurde mit Unterstützung der Vereinigung indigener Cabildos des Amazonas-Trapezes - ACITAM, des Hauptsitzes von SENA Leticia, der Beratung der Julio Mario Santo Domingo-Stiftung, sowie  der Universität Francisco José de Caldas im Umweltfragen geplant.
