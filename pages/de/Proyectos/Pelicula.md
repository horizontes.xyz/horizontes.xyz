---
identifier: pelicula
layout: layouts/item.njk
permalink: /{{ parent | slug }}/{{ navtitle | slug }}.html
navtitle: 'Film: "Die Herkunft des Tikuna - Volkes"'
img: poster_film.jpg
---

# "Die Herkunft des Tikuna - Volkes" - Die Rettung eines Mythos und einer Sprache

Um die Ureinwohner Kolumbiens sichtbar zu machen, hat die CHC zusammen mit dem Stamm den ersten ethnografischen Spielfilm “El Origen del Pueblo Tikuna” - „Die Herkunft des Tikuna-Volkes“ 2008 gedreht, welcher bei den “Big Apple Latin American Cinema Awards” 2009 in New York, als bestes Erstlingswerk  und in der ICT Preisverleihung des kolumbianischen Kulturministeriums ausgezeichnet wurde.

Dies ist eine Initiative des indigenen Stammes Nazareths / Amazonas, welches als Projekt des IBURI-Kanals (geschlossener Fernsehsender der Gemeinschaft) entstanden ist.

Der Film erzählt nach althergebrachten Überzeugungen die Herkunft dieser ethnischen Gruppe, die sich über Brasilien, Kolumbien und Peru erstreckt und mit diesem Film ein audiovisuelles Gedächtnis der über Jahrhunderte mündlich offenbarten Tradition schafft.

Dieser Film, der unter Beteiligung der Ureinwohner Nazareths gedreht wurde, ist die erste Produktion in Kolumbien, die in einer anderen Sprache als Spanisch aufgenommen wurde und bei dem mehr als 60% des technischen Personals Ureinwohner waren. Er erhielt eine Auszeichnung des Kulturministeriums in der IKT-Modalität mit sozialer Einbeziehung (2008), die Unterstützung der Universität Politécnico Grancolombiano und der CHC. Er wurde in Spanisch, Deutsch, Englisch, Französisch, Portugiesisch und Italienisch untertitelt. Es ist erwähnenswert, dass der Film ein sichtbarmachendes Element der Amazonas-Ethnien geworden ist.

 - [TRAILER](http://www.youtube.com/watch?v=zYQb4f-CaGU)
 - [Spielfilm](https://www.youtube.com/watch?v=cN-SG6nASLQ&pbjreload=10)

Hinter den Kamaras
 - [Forschung und Entwicklung des Films](https://www.youtube.com/watch?v=y85Z83XJYLE)
 - [Vorproduktion](https://www.youtube.com/watch?v=gbkFmHw5WXo)
 - [Produktion](https://www.youtube.com/watch?v=z64X-gx5oj4)
 - [Postproduktion](http://www.youtube.com/watch?v=KiueXm8wX84)


<div class="fr pa-l">
<img src="/static/Pelicula/1.jpg" />
</div>


**Vom Amazonas zum Big Apple
Von einer Minderheit für die Welt
"Der Ursprung des Tikuna-Volkes"**


<div class="fl xpa-r mw-5">
<img src="/static/Pelicula/2.jpg"/>
</div>


Die Jury des „Big Apple Latin American Cinema Awards“, in New York hat am 17. September 2009 den Spielfilm „Die Herkunft des Tikuna-Volkes“, der den Schöpfungsmythos der indigenen Völker des Amazonas Trapezes erzählt, mit der Trophäe in der Kategorie „Bestes Erstlingswerk“ ausgezeichnet.

<div class="fr pa mw-5">
  <img src="/static/Pelicula/3.jpg"/>
</div>

Dieser Film, der die Herkunft einer ethnischen Minderheit der Amazonasländer enthüllt und deren Akteure Einheimische dieser ethnischen Gruppen sind, wurde von den Ältesten des Stammes als sagral benannt und kann nun in vielen Szenarien gesehen werden, wo man die kulturellen und künstlerischen Manifestationen zu schätzen weiss.
Für die CHC ist es ein Stolz, mit der breiten Öffentlichkeit, mit jenen, die ihre Arbeit unterstützen, und mit jenen Menschen, die kaum von ihr gehört haben, die Freude zu teilen, die sich in den Herzen ihrer Protagonisten, Angehörigen der indigenen Gemeinschaft, ihres Direktors (Gustavo de la Hoz) und deren Produzentin (Dorothea Wolf Nurnberg) befindet.
Herzlichen Glückwunsch an die einheimischen Schauspieler, die ihre Geschichte für diejenigen interpretierten, die sie nicht kannten.
Das CHC realisierte Präsentationen in folgenden universitären und kulturellen Räumen:

**Uraufführung des Spielfilms "Die Herkunft der Tikuna - Volkes" im Tikuna - Stamm Nazareth - Amazonas**

<div class="fl pa-r">
<img src="/static/Pelicula/4.jpg"/>
</div>

Anfang Februar 2009 wurde der Film exklusiv im Indianerstamm Nazareth-Amazonas gezeigt; ein Akt voller Magie, Freude und Optimismus, an dem die Protagonisten, die Techniker, die Bewohner des Stammes und vier von der CHC geladenen Professoren der Universität Javeriana Bogotás teilnahmen. Dies war das erste Mal, dass die Tikunaindianer einer ihrer sagralen Mythen auf der Leinwand anschauen konnten.


**Erfolgreiche Uraufführung des Spielfilms "Die Herkunft des TIKUNA - Volkes" an der Universität Politécnico Grancolombiano in Bogotá mit Persönlichkeiten aus Kunst und Kultur und einigen Protagonisten dieser Filmarbeit.**

<div class="fr pa-l mw-15">
<img src="/static/Pelicula/6.jpg"/>
</div>

Der vom Kulturministerium  ausgezeichnete Spielfilm wurde am 28. April 2009 in Anwesenheit des bogotaner Teams, seines Direktors Gustavo de la Hoz, der Produzentin Dorothea Wolf-Nurnberg und zwei der Protagonisten, Juvencio Pereira Manduca und Yenika Mojica Pereira der Ethnie Tikuna, im Auditorium der Universität Politécnico Grancolombiano projiziert.
<div class="fl pa-r mw-10 ">
<img src="/static/Pelicula/7.jpg"/>
</div>

Dr. Pablo Michelsen Niño, Rektor des Politechnikums, bezeichnete die Produktion als ein Beispiel für die Arbeit der sozialen Projektion dieser wichtigen Bildungseinrichtung und als eine Widerspiegelung dessen, was sie in der Kinematographie mit Anwendungen der akademischen Tätigkeit im nationalen Umfeld geleistet hat.

<div class="fr pa-l mw-10">
<img src="/static/Pelicula/8.jpg" />
</div>

Carlos Augusto García, Dekan der Fakultät für Kommunikations- und Kunstwissenschaften, zeigte sich mit dieser Arbeit zufrieden, die in seinen Worten die Pädagogik mit einer Übung zur Schaffung und Präsenz des kollektiven Gedächtnisses mit dem Mythos der Schöpfung des Tikunavolkes des Amazonas erneuert und neu erfindet.

<div class="fl pa-r mw-8">
<img src="/static/Pelicula/9.jpg" />
</div>

Das Kulturministerium hob durch Germán Franco hervor, wie dieser Film die kulturelle Vielfalt des Landes zeigt, und würdigte die Einbeziehung derer, in diese großen filmischen Bemühungen, die lange Zeit ausgeschlossen waren.
Silsa Arias, Mitglied der Vertretung der National Indigenen Organisation Kolumbiens (ONIC), glänzte mit herausragender Beteiligung und betonte, wie wichtig es sei, die Akademie für diejenigen zu öffnen, die in unserer Gesellschaft unsichtbar waren, damit sie aus der Sichtweise ihres Volkes einen Beitrag zur kolumbianischen Gesellschaft leisten können . Sie lehrte uns, dass der Mythos für indigene Völker Realität sei.
<div class="fr pa-l w-10">
<img src="/static/Pelicula/10.jpg" />
</div>
Es blieb uns, unseren Dank an den Tikuna-Stamm auszusprechen, der unter der Leitung der Corporación Horizontes Colombianos ihre Türen für das Arbeitsteam geöffnet hatte, sich mit seinen Mitgliedern integrierte, um schliesslich zu diesem Spielfilm zu gelangen und der in den verschiedenen Szenarien, in denen er gezeigt wurde, viel zu erzählen hatte.
Am 5. Mai 2009 wurde der Film in zwei Funktionen der gesamten Universitätsgemeinschaft mit ihren jeweiligen Filmforen vorgestellt.


**Die Zuá-Stiftung genießt „Die Herkunft des Tikuna-Volkes“**
<div class="fl pa-r w-15">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/11.jpg'">
<img src="/static/Pelicula/11.jpg" />
</a>
</div>
Am 23. Mai 2009 wurde der Film in der Zuá-Stiftung in Sassaima-Cundinamarca unter Beteiligung der Ureinwohner gezeigt, die zum Teil in dem Film mitwirkten und diese Erfahrung mit den Begünstigten der Zuá-Stiftung teilten. Es handelt sich um ausgegrenzte Personen, Bewohner von Patio Bonito, die dank der Stiftung die Möglichkeit haben, eine berufliche Laufbahn einzuschlagen. Dieser Austausch hinterließ für beide Seiten eine Lehre. Die Generaldirektoren beider gemeinnütziger Organisationen nahmen ebenfalls an diesem Treffen teil.


**“Die Herkunft” an der Nationalen Universität Kolumbiens - Sitz Medellín**
<div class="fr pa-l w-10">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/12.jpg'">
<img src="/static/Pelicula/12.jpg" />
</a>
</div>
Am 1. August 2009 wurde der Film einer kleinen Gruppe von Studenten vorgestellt, insbesondere Angehörige indigener Stämme verschiedener Ethnien und Regionen des Landes. Nach Abschluss der Projektion wurde ein sehr interessanter Dialog über die Realisierung und deren Zweck entwickelt.

**“Die Herkunft des Tikuna - Volkes" bei der Eröffnung des Charles Darwin Symposiums; die Universität Tadeo Lozano in Bogotá würdigt den Mann der Wissenschaft**

<div class="fl pa-r w-10">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/13.jpg'">
    <img src="/static/Pelicula/13.jpg" />
  </a>
</div>

Am 18. August 2009 wurde im Auditorium Maximum der Tadeo Lozano Universität der Film “Die Herkunft” im Rahmen des Symposiums präsentiert, das von den Fakultäten der Geisteswissenschaften, der Meeresbiologie und dem Zentrum für Kunst und Kultur organisiert wurde.
Es fand ein Forum statt, in dem Mauricio Laurence als Moderator fungierte, unter Teilnahme von Gustavo de la Hoz, Regisseur des Films, und Yenica Mojica Pereira, eine der Hauptdarstellerinnen der Ethnie Tikuna. Hervorzuheben,  war die massive Beteiligung der Studenten und Dozenten und ihr Interesse, das Thema genauer zu betrachten.

**Der "Ursprung des Tikuna-Volkes" im alternativen Raum von Uniandinos -
Absolventen der Universität Los Andess**
 <div class="fr pa-l w-10">
 <img src="/static/Pelicula/14.jpg" />
 </div>
Am 8. September 2009 wurde in Uniandinos der Film der Corporación Horizontes Colombianos gezeigt, organisiert von der Sozialen Allianz und der Kulturabteilung Uniandinos als Homenage an die Tikuna-Kultur.
Zu dieser Präsentation wurden Miguel Dionisio Ramos, Juvencio Pereira Manduca, Yenica Mojica Pereira und Rosenaida Dionisio Quirino, Hauptdarsteller des Films, sowie die Produzentin Dorothea Wolf - Nurnberg und der Regisseur Gustavo De La Hoz eingeladen, die nach der Vorführung am Forum teilnahmen. Begleitend wurde eine Kunsthandwerksausstellung organisiert.

**"Die Herkunft des Tikuna - Volkes" für Film- und Fernsehstudenten**   
<div class="fl pa-r w-15">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/15.jpg'">
<img src="/static/Pelicula/15.jpg" />
</a>
</div>
Am 6. Oktober 2009 genossen die Film- und Fernsehstudenten die Vorführung in den Räumen der Lumière Foundation. Diese Filmproduktion hinterliess einen großen Eindruck auf die zukünftigen Filmemacher und sie verstanden die neue Anregung in der kolumbianischen Kinoszene.
Das Kinoforum wurde als Sitte und Notwendigkeit angeboten, um viele Bedenken durch die indigenen Assistenten, dem Regisseur und der Produzentin auszuräumen.


**Auseinandersetzung des Films "Die Herkunft des Tikuna - Volkes" mit Anthropologen, Soziologen und Historikern**
<div class="fr pa-l w-10">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/16.jpg'">
<img src="/static/Pelicula/16.jpg" />
</a>
</div>
Am 8. Oktober 2009 wurde der Spielfilm für Studierende der Studiengänge Anthropologie, Soziologie und Geschichte an der Universität Pontificia Javeriana vorgestellt.
Aus zeitlichen Gründen konnte das erwartete Kinoforum nicht abgehalten werden, es wurde jedoch ein unkonventionelles Gespräch geführt.


**"Die Herkunft des Tikuna - Volkes" und die Umwelt**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/17.jpg" />
</div>
Am 16. Oktober 2009 wurde an der Universität Francisco José de Caldas in Bogotá der Film für die Studenten der Studiengänge verbunden mit der Umwelt vorgestellt.
„Nach 517 Jahren (Entdeckung Amerikas) sind unsere Gegenwart, unsere Gewalt, unsere Vergesslichkeit und unser Verhalten die Folge der Akkulturation in Amerika. Das Leiden unserer indigenen Bevölkerung und ihre gegenwärtigen Qualen zeigen an, dass die historische Verfolgung unserer Wurzeln noch längst nicht vorbei ist.
“Die Rettung unserer indigenen Wurzeln ist von entscheidender Bedeutung für unsere lebendige Vergangenheit und unsere ungewisse Zukunft ...” Rafael - Student der Karriere Umweltverwaltung.

**Zweite Präsentation an der Pontificia Universität Javeriana**
<div class="fr pa-l w-15">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/18.jpg'">
<img src="/static/Pelicula/18.jpg" />
</a>
</div>
Diese Präsentation der "Herkunft des Tikuna - Volkes" am 19. Oktober 2009 war öffentlich und fand im Troncoso Marine Auditorium der Universität Pontificia Javeriana statt.
Studenten verschiedener Universitäten Bogotás sahen den Film und tauschten Meinungen im Forum aus. Diese Projektion wurde von den Tikunas und Vertretern der CHC begleitet. Auch eine Ausstellung der Ahnenkunst wurde realisiert.


**Die Herkunft des Tikuna - Volkes" beim V. Internationalen Filmfestival von Pasto - Nariño**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/19.jpg" />
</div>

Im August 2009 fand in Pasto das Treffen der Schamanen, Mamos und Taitas (1. Internationales Treffen der Kulturen der Anden und des Pazifiks) statt, bei dem Wissen ausgetauscht und Konferenzen über traditionelle Medizin abgehalten wurden, ohne die Zeremonien, Rituale, Lieder und Tänze, unter anderen künstlerischen Manifestationen zu vernachlässigen.
Die teilnehmenden Länder waren: Chile, Venezuela, Argentinien, Bolivien, Peru und Ecuador.
Zur gleichen Zeit fand das V. Internationale Filmfestival Pastos statt, an dem Fachleute aus Film und Fernsehen sowie nationale und internationale Redner teilnahmen.
<div class="fr pa-l w-15">
  <img src="/static/Pelicula/20.jpg" />
</div>

Die Corporación Horizontes Colombianos war durch Dorothea Wolf-Nurnberg als Ehrengast, mit dem Film "Die Herkunft des Tikuna - Volkes", als Film außerhalb des Wettbewerbs vertreten.
 Dorothea, die auch Produzentin des Films ist, wurde zur Gesprächsrunde eingeladen, an dem ebenfalls die Dokumentarfilmerin Martha Rodríguez aus Kolumbien, Juan Martín Cueva, Direktor des Dokumentarfilmfestivals "Zero Latitude" Ecuadors, Humberto Mancilla, Direktor des Menschenrechtsfilmfestivals Boliviens und der Gouverneur des Departments Nariños, Antonio Navarro Wolff, an der Universität Nariño, teilnahmen. Der Schwerpunkt lag auf dem Thema Erinnerung und audiovisuelle Produktionen.

**Einladung zum 2. Internationalen Festival für Alternativ- und Gemeinschaftsfilm und Video "Ojo al Sancocho" in Bogotá D.C.**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/21.jpg" />
</div>

Am 22. September 2009 wurde die Corporación Horizontes Colombianos mit dem Film „Die Herkunft des Tikuna - Volkes“ zur zweiten Ausgabe des Internationalen Festivals für alternative und gemeinschaftliche Film und Videoproduktionen „Ojo al Sancocho“ im südlichen Stadtteil Ciudad Bolívar in Bogotá eingeladen. An diesem Nachmittag wurde der Trailer des Films zusammen mit einem Gespräch mit Delegierten indigener und afro-nachkommender Völker aus Bolivien und Kolumbien präsentiert. Ebenso nahmen die Generaldirektorin der CHC und  Produzentin des Films sowie der Regisseur Gustavo De La Hoz an diesem Dialogtisch teil.

**1. Indigene Film- und Videoausstellung in Kolumbien „Daupará“ - Jenseits sehen**
<div class="fr pa-l w-15">
<img src="/static/Pelicula/22.jpg" />
</div>

Dieses Projekt ist Teil eines gemeinsamen Vorschlags der indigenen Gemeinschaften und Völker in ganz Amerika. Heute, mehr denn je, wo unsere Mutter Erde vor sich hinstirbt, schließen sich Stimmen, Gefühle und Kämpfe indigener Völker ihrer Verteidigung an. Durch die erste indigene Film- und Videoausstellung in Kolumbien wurden wir aus ihrer Sichtweise in das kolumbianische kollektive Vorstellungsvermögen zurückgeführt. Durch die erste indigene Film- und Videoausstellung in Kolumbien, deren Basis ein mehrfarbiger Rucksack war, der mit audiovisuellen Früchten gefüllt war, die von Mitgliedern der indigenen Völker und Gemeinschaften unseres Landes in den letzten Jahren unter dem Impuls und der Abstimmung von siegreichen Produktionen hergestellt und überarbeitet wurden und die beim IX. Internationalen Festival für Kino und Video der indigenen Völker, das 2008 in Bolivien entwickelt wurde, sieggekrönt hervorgingen.
Die Corporación Horizontes Colombianos wurde mit ihrem Film "Die Herkunft des Tikuna – Volkes” und zu vier Arbeitstischen eingeladen, in denen folgende Themen erörtert wurden: Erfahrungen in der indigenen Kommunikation, Wissensaustausch zwischen indigenen und nicht-indigenen Filmemachern, öffentliche Politik und ethnische Kommunikation, Medien und ethnische Repräsentation. Der Film wurde an der Nationalen Universität Kolumbiens im Rahmen des Filmfestivals vorgestellt.

**1. San Agustín Film- und Videofestival**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/23.jpg" />
</div>
Dieses im Entstehen begriffene audiovisuelle Medienereignis fand vom 13. bis 16. November 2009 statt. Es dauerte vier Tage, um die Entwicklung des neuen kolumbianischen Kinos mit Schwerpunkt auf ethnografischem und sozialem Kino und Video zu würdigen.
Während der im Rahmen des Festivals stattfindenden Seminare, Workshops und Gespräche hatten die Besucher Gelegenheit, mit Regisseuren, Drehbuchautoren und Wissenschaftlern ins Gespräch zu kommen.
Auf dem Programm stand „Die Herkunft des Tikuna - Volkes“ sowie audiovisuelle Produktionen aus Mexiko und Kolumbien.

Der Film wurde auch in kulturellen und universitären Räumen sowie auf Filmfestivals in Bolivien, Deutschland, Italien, Frankreich und Spanien gezeigt.
