---
identifier: Educacion
permalink: /{{ locale }}/{{ parent | slug }}/bildung.html
title: Bildung – Basis für ein würdevolles Leben
img: proy_edu_foto_1.jpg
notopimg: true
layout: layouts/page.njk
---


## Vorgeschichte

Durch die Ausbildung von Fachleuten in den verschiedenen Wissensgebieten versucht die Corporación Horizontes Colombianos bis heute, die Bevölkerung zu stärken und indigenen Jugendlichen den Zugang zu Universitätsstudien in verschiedenen Studiengängen zu ermöglichen, die sie selbst wählen.

![](/static/proy_edu_foto_1.jpg)

Innerhalb des Programms haben 15 Ureinwohner verschiedener kolumbianischer Ethnien, Frauen und Männer, ihre Hochschulbildung mit vollständiger und teilweiser Unterstützung abgeschlossen und ihre Verpflichtung gegenüber der CHC erfüllt, indem sie ihr erworbenes Wissen innerhalb ihrer Herkunftsgemeinschaften weitergeben und ihre Erfahrungen in Städten wie Bogotá, Palmira und Medellín, während ihres Studienaufenthaltes teilen.

Derzeit arbeiten wir daran, diese Initiative in andere Stämmen des Landes zu bringen.

{% set path = identifier + '/Antecedentes' %}
{% set pics = 8 %}
{% include "components/gallery.njk" %}

## Aktualität

Bildung als Grundrecht sichert insbesondere ethnischen Minderheiten wie indigenen und afro-nachkommenden Gemeinschaften eine aktive Teilnahme an der sozialen und kulturellen Entwicklung des Landes. Seit Jahrhunderten an den Rand gedrängt, leiden sie aufgrund der immensen Gleichgültigkeit unter Gewalt und Missachtung ihrer Kultur seitens der Gesellschaft. Durch den Zugang zu Universitätsstudien haben die Menschen die Möglichkeit, ihre Kultur bekannt zu machen und auf dem gleichen Chancenniveau wie ihre weißen Mitstudenten zu sein, ihre intellektuellen Fähigkeiten auszuüben und das Wissen ihrer Vorfahren zu teilen.

![](/static/proy_edu_foto_2.jpg)

Derzeit unterstützt die CHC 5 Studenten (Frauen und Männer aus dem Nasa Stamm des Caucas) in verschiedenen Studiengängen an der Universidad Surcolombiana der Stadt Neiva - Huila. Junge verantwortungsbewusste Führungskräfte haben die Aufgabe, innerhalb den Gemeinden denen sie angehören, mittels ihres erworbenen Wissens, Beratung zu leisten.
Unser Ziel für 2020 ist es, zwei weitere Studenten zu unterstützen und das Programm schrittweise mit einer größeren Anzahl verbundener Studenten zu erweitern.

{% set path = nil %}
{% set pics = 14 %}
{% include "components/gallery.njk" %}
