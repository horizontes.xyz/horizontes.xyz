---
identifier: Intercambioetnico
permalink: /{{ locale }}/{{ parent | slug }}/austausch.html
title: Von der fernen Morgendämmerung bis zum Erwachen des neuen Kontinents
img: intercambioetnico_foto_1.jpg
layout: layouts/gallery.njk
pics: 12
---

Forschungsvorschlag - Dokumentarfilm: Konvergenzpunkte zwischen verschiedene ethnische Gruppen Kolumbiens und Chinas.
Das ethnisch-kulturelle Austauschprojekt konzentriert sich auf die Untersuchung, Dokumentation und Herangehensweise der verschiedenen Konvergenzpunkte zwischen den Volksgruppen Kolumbiens und Chinas, mit denen die CHC gemeinsame Arbeit leistet. Diese Initiative zielt darauf ab, die Brüderlichkeitsbeziehungen und den Austausch zwischen den Ureinwohnern der beiden Kontinente zu stärken und gemeinsam Anstrengungen zu unternehmen, die es uns ermöglichen, durch die Kultur die Gemeinsamkeiten und ethnografischen Unterschiede zu verstehen, aber auch die Formen des kollektiven Denkens, die bei der Arbeit helfen im Einklang mit der Natur und anderen Bewohnern dieser Erde zu leben.
