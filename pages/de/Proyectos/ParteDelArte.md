---
identifier: ParteDelArte
title: Parte del Arte – Galerie
img: parte_del_arte_foto_2.jpg
notopimg: true
layout: layouts/gallery.njk
pics: 9
---

<img src="/static/parte_del_arte.jpg"/>

Die Corporación Horizontes Colombianos CHC begann im Oktober 2003 ihre kulturelle Arbeit „Parte del Arte - Galerie“, der sich zu dieser Zeit im CHC-Hauptsitz befand.

Dieses Projekt wurde mit der Absicht ins Leben gerufen, einen Austausch zwischen kolumbianischen und deutschen Künstlern herzustellen. Es verfügte vier Jahre lang über Räumlichkeiten, in dem mehrere kollektive und individuelle Ausstellungen kolumbianischer und deutscher Künstler stattfanden, darunter Carlos Rojas, Edgar Negret, Dario Ortiz, Doris Hinzen-Roehrig, Umberto Giangrandi, Mario Salcedo, Jim Amaral, Gladys Ortiz und viele andere Künstler und Künstlerinnen.

Diese Räume waren offen für Ausstellungen von Künstlern mit Behinderungen wie Aydée Ramírez, weniger bekannten aber ausgezeichneten Künstlern und auch für die Ausstellung von Abschlussarbeiten seitens Studenten der Pontificia Universidad Javeriana.

Die Galerie war bekannt für einen ständigen Austausch und für ihre künstlerischen und sozialen Aktivitäten.

Ausstellungen der CHC:

"Die Kinder zählen auch." Gemeinschaftsausstellung, organisiert von der CHC und UNICEF Kolumbien.

"Identität - Realität." Kollektive Ausstellung. Eine Hommage an den Künstler Carlos Rojas unter Beteiligung von Gladys Ortiz, Mario Salcedo und Werken des Meisters Rojas.

"Minotaurus Konzert". Einzelausstellung von Nubia Medina.

"Ankunft im unruhigen Meer". Ausstellung des deutschen Künstlers Uwe Anders.

"Visuelle Kontraste." Einzelausstellung der Künstlerin Mari Rincón aus Medellín.

"Die Stille des Lichts." Einzelausstellung des Künstlers Germán Méndez.

"Jene die sich bemächtigen." Einzelausstellung sozialen Charakters, Mund-Malerin Aydée Ramírez.

"Horizont." Ausstellung der deutschen Künstlerin Doris Hinzen Roehrig aus Berlin.

"Frauen in der Kunst." Gemeinschaftsausstellung von 4 Frauen im ECOPETROL Club, organisiert von der CHC.

"Konkrete Vision." Einzelausstellung der Künstlerin Raquel Ramírez aus Bucaramanga.

"Ein Blick von Herzen." Einzelausstellung des Künstlers Jhon Jairo Restrepo aus dem Quindío für ein blindes und sehendes Publikum.

"Was wir drinnen tragen." Ausstellung der deutschen Künstlerin Petra Jung, begleitet von einer audiovisuellen Installation der kolumbianischen Künstlerin Antonia García.

"Innenansicht - Außenansicht". Gemeinschaftsausstellung in Berlin - Deutschland mit Beteiligung der kolumbianischen Künstlerin Mari Rincón aus Medellín, von der CHC mitorganisiert.

"Engel in der Kunst." Gemeinschaftsausstellung mit Beteiligung kolumbianischer und deutscher Künstler.

Ausstellung von drei Diplomarbeiten von Studenten der Bildenden Kunst der Pontificia Universidad Javeriana.

"Ein Blick von Herzen." Wanderausstellung des Künstlers Jhon Jairo Restrepo, organisiert von der CHC in Cali / Valle.

"Grabve - schrecklich." Gemeinschaftsausstellung von drei kolumbianischen Künstlern, die ihre Kupfersticharbeiten zeigten.

"Erinnerungen an die Stadt." Ausstellung von drei studentischen Künstlern; Diplomarbeiten der Bildenden Kunst der Pontificia Universidad Javeriana.

"Panorama Vision." Gemeinschaftsausstellung kolumbianischer, deutscher und nordamerikanischer Künstler.

"Badezimmererlebnisse ..." Einzelausstellung einer Künstlerin mit ihrer Diplomarbeit in Bildender Kunst der Pontificia Universidad Javeriana.

"Ancestral Heritage" - Große Ausstellung indigener Kunst, begleitet von Fotografien, Videos und Klängen des Amazonas, unter Beteiligung von Studenten verschiedener Universitäten in Bogotá.

"Auf der Suche nach Fußspuren." Ausstellung des kolumbianischen Bildhauers Oscar Pulido; er lebt und arbeitet in Köln und stellte zum ersten Mal in Kolumbien aus.

"Kontinuität der Natur durch Stadtentwicklung". Gemeinschaftsausstellung des honduranischen Künstlers Kevin Nazar und der kolumbianischen Künstlerin Maria Andrea Umaña.

"Natürliche Anspielung." Einzelausstellung der Künstlerin Angélica Chaparro; ehemalige Studentin der Nationalen Universität Kolumbiens.

"Kosmische Explosion." Einzelausstellung des Künstlers César López seines neuen Aquarellwerks, begleitet von einem Videoclip der CHC "Fünf Variationen und ein Requiem".

Schließung des Raums „Parte del Arte - Galerie“ im Jahr 2007 aufgrund mangelnder finanzieller Unterstützung.

Die CHC hat mit diesem Projekt großartige Arbeit geleistet, indem sie Menschen aller Schichten einschloss, sowie Künstler als auch Besucher, und indem sie in vielen Personen das Interesse für eine der bildenden Künste weckte, die sich gegen Gewalt auflehnt, die im Land herrscht.

Wir konnten Spenden für die CHC sammeln und gleichzeitig Künstler unterstützen, die sich zunehmend in einer komplizierten Situation befinden. Ausserdem konnten wir die Menschen erfreuen, die unsere Galerie besuchten.

Durch "Parte del Arte - Galerie" erfuhren viele Menschen über das soziale Engagement der Corporación Horizontes Colombianos.

Es ist beabsichtigt, das Projekt per Internet wieder aufzunehmen.

<video controls>
 <source src="/static/parte_del_arte.mp4" type="video/mp4">
</video>
