---
identifier: ArteAncestral
layout: layouts/gallery.njk
title: Ahnenkunst
img: ArteAncestral/10.jpg
notopimg: true
pics: 10
---


<div class="fr pa-l">
<img width="256px" src="/static/tienda_en_linea.jpg"/>
<h3>Online-Shop</h3>
</div>

Projekt zur Förderung des Kunsthandwerks verschiedener indigener Völker

Das Projekt geht auf den Vorschlag der Tikuna- Stämme und der CHC "Genossenschaft für indigene Kunstgewerbetreibende des Amazonas - Trapez" zurück, welches die Eingliederung indigener Gemeinschaften in produktive Räume zum Ziel hatten, ebenso die Kultivierung der Assoziierung und des Genossenschaftswesens, um die Ahnenarbeit der Stämme zu retten, zu erhalten und zu fördern.
Mit der Absicht, dieses Projekt zurückzuerobern und auf andere Stämme auszudehnen, mit denen die CHC zusammenarbeitet, sollen Vertriebs- und Verkaufskanäle für das Handwerk durch Allianzen und unter dem Konzept des fairen Handels geöffnen werden, um somit das Einkommen und die Lebensbedingungen der Kunsthandwerker in den verschiedenen Stämmen zu verbessern.
Dieses Projekt wird mit Unterstützung der Vereinigung der entsprechenden indigenen Cabildos und Einrichtungen wie ACITAM, der Universität Francisco José de Caldas und des Studentausschusses der Universität Surcolombiana durchgeführt.
Gegenwärtig bemüht sich die CHC, verschiedene traditionelle Kunsthandwerker zu ermutigen, um sich einer interaktiven Plattform anzuschliessen, auf der sie ihre Arbeiten ausstellen und Verbündete innerhalb des Fair-Trade-Systems suchen können.
