---
identifier: EncuentrosVivenciales
title: Erlebnisbegegnungen
img: encuentrosvivenciales_foto_1.png
layout: layouts/gallery.njk
pics: 8

---

Das Projekt entstand nach tiefgreifenden Reflexionen hinsichtlich des Teilens, Vernetzens, Versöhnens und des Sich-Selbst-Kennenzulernen.

Abseits der Natur, in lauten, luftverschmutzten Städten und stressigen Gewohnheiten unterlegen, verurteilen wir uns, den Sinn für das Wesentliche des Lebens zu verlieren und Gefangene unserer eigenen Erfindungen zu werden.

Bei erlebnisorientierten Treffen können die Teilnehmer Wissen und Praktiken inmitten der Natur mit Gemeinschaften austauschen, die daran arbeiten, diese magischen Orte zu bewahren, sowie die Stille genießen und diese Atmosphähre zu einer umfassenden, tiefen und intimen Selbstbeobachtung der Welt und des Lebens nutzen.

Zielgruppe sind öffentliche und private Unternehmen, Bildungseinrichtungen und die breite Öffentlichkeit mit der Notwendigkeit zur Entspannung und um das Zeitgefühl zu erweitern.

Das Wandern in den Berge, das Besuchen der Lagune, gesundes Essen und das Teilen von Wort und Musik um das  Feuer herum, werden uns zu Meditation und somit zu einem körperlichen und geistigen Wohlbefinden führen, um gestärkt und harmonisiert nach Hause zurückzukehren.
