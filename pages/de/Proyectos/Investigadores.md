---
identifier: Investigadores
title: Forscher in der Schulzeit
img: investigadores_foto_1.jpg
---
Das Projekt geht auf die Initiative zurück, Schüler der zehnten und elften Klasse der pädagogischen Einrichtung José Reyes Pete, Sitz – technisches, ethno-ökologisches Institut „Cxamb Wala“ (Pueblo Grande), im indigenen Stamm Vitoncó, Páez - Cauca  zu unterstützen, welche ethno – ökologische Vorschläge als Abschlussvoraussetzung entwickeln. Es besteht ein großes Interesse daran, vom Gefühl der indigenen Jugend her zum kulturellen Überleben beizutragen. Aus diesen Initiativen retten sie ihre eigenen Traditionen und Kenntnisse, welche nach Weltanschauung und Überzeugungen untersucht werden, in Begleitung der Ältesten und Stammesleiter und wodurch die Kommunikation zwischen den Generationen gestärkt wird. Auf diese Weise wird der Weg geebnet, um Räume für die Beteiligung der Jugend zu schaffen, die aus kultureller Autonomie entstanden sind und in denen ein tiefer Respekt für Mutter Erde, das Territorium und die darin lebenden Geister besteht. Alle Maßnahmen und Aktionen, die entwickelt werden, um die Ziele zu erreichen, priorisieren das WËT FIZENXI, was bedeutet: Gut leben, um in Harmonie zusammen zu wohnen.

<video width="90%" controls>
 <source src="/static/Presentación_Deny.mp4" type="video/mp4">
 Ihr Browser unterstützt das Video-Tag nicht.
</video>

Die -CHC- erfreut sich, den Prozess der Erstellung des traditionellen Geschichtensammelhefts finanziell unterstützt zu haben: „Yeckatê´we´sx (yuwetx katxude ec)“; eine Initiative der einheimischen indigenen NASA-Schülerin Denny Milena Cayuy, als Bachelor-Abschlussarbeit an der José Respetes Schule.
Die Unterstützung dieser Initiative ist eine gemeinsame Leistung, die darauf abzielt, die eigenen Traditionen und Kenntnisse zu stärken, den Erhalt der mündlichen und schriftlichen Sprache zu fördern und diese Geschichten vor allem den Kindern und Jugendlichen der indigenen Gemeinschaften zugänglich zu machen. Auf diese Weise werden die Geschichten mit redaktioneller Qualität und Bildern veröffentlicht, die von den Kindern und Jugendlichen der Gemeinschaft selbst erstellt wurden. Dies macht es zu einem Gemeinschaftsprozess, der ihren Interessen und Bedürfnissen angepasst ist, wie beispielsweise der Bewahrung ihrer eigenen Kultur, im Kontext der Globalisierung, die dazu neigt, Kulturen zu kommerzialisieren und zu homogenisieren. Aus diesem Grund ist es sehr wichtig, die eigene Identität durch diese Art von Initiativen zu stärken, die darauf abzielen, Kinder und Jugendliche zu kenntnisreichen und stolzen Multiplikatoren ihrer indigenen Geschichte zu machen.
Wir arbeiten derzeit an der Beschaffung von Ressourcen mit dem Ziel, eine gedruckte Version in Hartpaste und eine Analyse der Geschichten auf Spanisch zu erstellen, mit dem Ziel, die Arbeit in anderen Nasa-Reservaten und Bildungseinrichtungen in den Departements Cauca und Huila zu verteilen.
