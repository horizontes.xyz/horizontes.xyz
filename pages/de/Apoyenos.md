---
date: 2019-01-06
identifier: Apoyenos
title: Unterstützen Sie uns
layout: layouts/base.njk
permalink: /{{ title | slug}}.html
tags:
 - nav_de
---

# DER WERT DER SOLIDARITÄT

<main class="flex jc-c">
<div class="pa-l-2 pa-r">

<iframe width="888" height="666" src="https://www.youtube.com/embed/LFeuJWNyCHU" title="CHC- Apoyenos - Suport us" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Wählen Sie die Art der Unterstützung:</h2>
<h3>Patenschaft – Pa Chawene “Eine Hand meinem Bruder”</h3>
<a href="/static/Plan-Padrino_pa-chawene_de.pdf">Präsentation</a>
<br>
<h3>
Umfassende soziale Verantwortung der Unternehmen
"Bereitschaft, sich für die Gesellschaft zu engagieren
</h3>

- Was ist es?

Es ist persönlich, es ist leitend, es ist geschäftlich, es ist sozial,
es ist für alle!

- Was deckt es ab?

Es ist Kompromissbereitschaft, es ist ethisch, es ist freiwillig,
es ist Kultur, es sind Werte, es ist Zusammenarbeit. Es basiert
auf den Millenniumszielen.

- Wie lange?

Es ist nicht kurzfristig, es ist langfristig. Es strebt eine
nachhaltige Entwicklung in den Bereichen Gesundheit,
Bildung und Kulturbräuche, sozialer und wirtschaftlicher
Natur an.

- Wie?

Spenden in Zeit, Geld, Projektunterstützung.

</div>
<div class="ta-c" style="width: 256px">
    <img width="256px" src="/static/apoyenos.jpg"/>
    <a href="paypal.me/ApoyenosCHC">PayPal</a>
    <div>
        Für Geldspenden stehen folgende Bankkonten zur Verfügung, wir sind berechtigt Spendenquittungen auszustellen:

<b>In Kolumbien:</b>

Cuenta corriente Banco Colpatria
Corporación Horizontes Colombianos
N° 4431003061
Bogotá

<b>In Deutschland:</b>

Berthold Wolff
TARGOBANK
N° 0203653632
BLZ 30020900
    </div>
</div>
</main>
