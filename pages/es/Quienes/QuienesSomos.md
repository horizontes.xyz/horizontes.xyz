---
date: 2019-01-01
identifier: quienes
permalink: /quines-somos.html
title: Quienes Somos
tags:
 - nav_es
---
<center>

![](/static/Logo_CHC.png)
</center>

La Corporación Horizontes Colombianos – CHC - es una organización no gubernamental, que trabaja por el desarrollo de los pueblos originarios, la población afrodescendientes y campesina, convencida de la vital importancia que esto conlleva para la conservación del patrimonio natural, cultural y étnico.

Con el fin de reforzar la autonomía y la inclusión y asegurar la supervivencia de los Pueblos, nosotros venimos brindando herramientas de empoderamiento a las comunidades, para que ejerzan sus derechos fundamentales y se hagan artífices de su propio desarrollo, aportando así a la sociedad con su conocimiento, experiencia y sabiduría, los cuales asumen un rol cada vez más importante en la vida moderna, matizada por el abuso de los recursos naturales, los desastres ambientales y la desnaturalización de los valores.
