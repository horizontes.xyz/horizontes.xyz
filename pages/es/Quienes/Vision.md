---
date: 2019-01-02
identifier: vision
navtitle: Visión y Misión
permalink: /{{ locale }}/{{ parent | slug }}/{{ navtitle | slug }}.html
---
<div class="flex wrap justify-center">
<div class="item pa-r">

![](/static/mission.jpg)
# Misión
Posibilitar la vida digna y la autonomía de las comunidades originarias, afrodescendientes y campesinas a través del fortalecimiento de la educación y el fomento de proyectos encaminados a establecer puentes, gestionar recursos con el ánimo de generar soluciones económicas, ambientales y culturales, prioritarias para las comunidades.
</div>
<div class="item">

![](/static/vision.jpg)
# Visión
Desarrollaremos proyectos sociales y productivos que permitan lograr la autonomía y el mejoramiento de la calidad de vida de las minorías étnicas y campesinas a través de los encuentros vivenciales, la investigación, la práctica y creación artística, así como de iniciativas ambientales, culturales, tradicionales y rituales, que posibiliten la transformación de imaginarios y relaciones sociales.
</div>
</div>
