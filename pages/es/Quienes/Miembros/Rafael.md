---
identifier: rafael
title: Rafael Antonio Chaparro Torres
img: Miembros/Rafa.jpg
---

Tiene 30 años.
Región: Tota, Boyacá, Colombia
Administrador Ambiental con Maestría en Sistemas de Información Geográfica.
Descendiente de  abuelos indígenas-campesinos interesado por  las comunidades tradicionales y el conocimiento milenario  de los pueblos ancestrales latinoamericanos. Desde 2009 ha participado de diferentes iniciativas y programas socio-comunitarios relacionados con el pensamiento ancestral, la educación ambiental y el reconocimiento de territorios (ruta maya, ruta inca). Hasta el año 2014 director del Colectivo Pachakutia, organización juvenil  encaminada a la educación ambiental desde los valores ancestrales y el arte comunitario.
Miembro voluntario de la Corporación Horizontes Colombianos - CHC durante 14 años, colaborador en gestión y formulación de proyectos, así como en investigación y asuntos ambientales. Desde junio 2019 miembro asociado y Subgerente de la CHC.
