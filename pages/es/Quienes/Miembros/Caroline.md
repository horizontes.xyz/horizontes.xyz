---
identifier: caroline
title: Caroline Schiller
img: Miembros/Caroline.jpg
---

Profesora voluntaria en la escuela de Waldorf en Bogotá (Inti Huasi, “Casa del Sol”), enseñando inglés, alemán y colaborando en el grupo de apoyo, con niños discapacitados.

Doble grado en Administración de Empresas y Economía (Universidad de Glasgow, Escocia) Nació el 20 de mayo de 1995 en Múnich/ Alemania, aunque sus padres son de la República Checa.

Después de cuatro años de estudios de Economía y Negocios y varios trabajos en el campo (incluida la posición en la consultora Deloitte en Leeds y el puesto de ventas en Zionsville, Indiana con Southwestern Advantage), decidió cambiar su enfoque para trabajar en proyectos que no estaban relacionados con finanzas, ventas, comercio y negocios. El voluntariado en la Eco-granja Can Decreix (Cerbere, Francia) le dio la oportunidad de descubrir alternativas económicas, es decir, "Degrowth" y movimientos como Slow Food y Food Sharing, luchando contra el problema del desperdicio de alimentos y la pérdida del uso de alimentos locales.

lgunos de los otros lugares de voluntariado en Europa incluyen la Granja Eco Krishna en Lesmahagow (Escocia), Mirapuri (en Mirapuri-Coiromonte, Italia) y Menschliche Welt (Yoga Ashram en Alemania cerca de Ravensburg), cada uno de los cuales la ayudó a evidenciar la necesidad de trabajar en pro de la vida, en armonía con la comunidad y en coexistencia con la naturaleza. Actualmente, en la Escuela Waldorf "Inti Huasi" (Casa del Sol) su visión se ha extendido y enfocado en la educación, que también es su objetivo y pasión como miembro de la Corporación Horizontales Colombianos (CHC) en Colombia.
