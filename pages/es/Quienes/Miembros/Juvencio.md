---
identifier: juvencio
title: Juvencio Manduca
img: Miembros/Juvencio.jpg
---

Perteneciente a la Etnia Tikuna.

Nació en el Resguardo de Nazareth, Amazonas, el 16 de marzo de 1981. Estudió su primaria en la Concentración Escolar Nazareth (CEN) y el grado quinto lo cursó en el Instituto indígena San Juan Bosco.

Culminó su bachillerato y posteriormente participó en el taller de dirección y producción de cine y televisión “Imaginando nuestra imagen” – INI, dictado por el Ministerio de Cultura en la Universidad Nacional de Colombia – Sede Amazonia (2003). Ingresó al programa “La Educación - base para una vida digna” de la CORPORACION HORIZONTES COLOMBIANOS en el año 2006, luego cursó un semestre de fundamentación en la Universidad Nacional de Colombia, Sede Amazonia (2007) y el Curso Preuniversitario (2007).

Por dificultades económicas, inicialmente sólo pudo realizar el estudio medio vacacional, pero durante ocho años, después de la culminación de los estudios de bachillerato, ha compartido sus conocimientos con la comunidad, liderando iniciativas, junto con las autoridades tradicionales.

Estudió varios semestres Derecho en la Universidad Politécnico Grancolombiano, después de vincularse en enero de 2006 a la Corporación Horizontes Colombianos, como miembro honorario y desde enero de 2008 es asociado y secretario de la Asamblea General de la CHC.
Regresó a su comunidad y fue elegido Curaca (máxima autoridad), dos años consecutivos.

Su gran esfuerzo es respaldado por sus padres, Pedro Pereira Ramos y Ligia Manduca Guillermo, quienes se desempeñan como agricultores en el Amazonas. También está casado, su esposa es Luz Emerita Moran Ahue. Son padres de José Camilo Pereira Moran.
