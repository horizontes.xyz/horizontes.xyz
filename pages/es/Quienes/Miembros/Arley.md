---
identifier: arley
img: Miembros/Arley.jpg
title: Arley Buitrago Landázuri
---

Afrodescendiente, nacido 1985 en Bogotá. Es Licenciado en Ciencias Sociales y Magister en Estudios Artísticos con experiencia y vocación por el trabajo comunitario. Ha trabajado con la Escuela de Arte Social Videos y Rollos, el Colectivo Entre-tránsitos, el Movimiento Nacional Cimarron y la Corporación Horizontes Colombianos (hoy miembro asociado) durante los últimos 5 años. En el interés constante por articular los procesos artísticos culturales y comunitarios, ha desarrollado prácticas artísticas como el performance y el videoarte con un enfoque de género, intercultural e interdisciplinar. Ha publicado los artículos “Memorias de una agonía continuada”, “La práctica mágico religiosa del Candomblé en Bogotá” y “El cisma de habitar un cuerpo negro”.
