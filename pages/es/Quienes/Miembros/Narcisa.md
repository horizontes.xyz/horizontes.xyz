---
identifier: narcisa
title: Narcisa Bautista Ramos
img: Miembros/Narcisa.jpg
---

Pertenece a la etnia Tikuna
Estudiante bilingüe de Formación Democrática Política Indígena en la fundación caminos de identidad FUCAI. Líder indígena de la comunidad en Nazareth de la cual ha sido Vice-Curaca adelantando proyectos de infancia, juventud y mujer.
Ha realizado cursos de culinaria típica del Amazonas, de cooperativismo y deporte. Es miembro activo de la Organización ACITAM y de la Corporación Horizontes Colombianos -CHC- en asuntos de la mujer indígena.
Ha sido dos veces consecutivos Curaca (Máxima autoridad) de la su comunidad Tikuna del Amazonas.
