---
identifier: berthold
title: Berthold Wolff
img: Miembros/Berthold.jpg
---

Nació el 2 de marzo de 1965 en Hilden/ Alemania. Terminó su Bachillerato en el Gimnasio Helmholz en Hilden Alemania en 1984. Estudio de teología en Bonn y en Viena/ Austria obteniendo su diploma en 1989.

Se ordenó como sacerdote en Colonia en 1992, luego de dos temporadas de trabajo como capellán, en Brühl y Bensberg.
Desde el año 2000, pastor en el Gimnasio del Arzobispo de Santa Úrsula y en la Isabel de Turingia Realschule en Brühl.

En septiembre de 2009 fue nombrado párroco en la parroquia de San Maximiliano Kolbe en Colonia Porz y 10 años después como pastor principal de la sala de transmisión 'Porz'.

Desde 1993 es miembro de la  fundación y Representante Legal de la entidad sin ánimo de lucro para el apoyo de educación de jóvenes en la Republica Dominicana (AJD e.V.) Como parte de esta actividad, desde el año 2000 ha estado viajando con un grupo de personas interesadas a la República Dominicana cada tres años.
