---
identifier: doro
title: Dorothea Wolf – Nuernberg
img: Miembros/Doro.jpg
---

Nació el 9 de enero de 1962 en Alemania, con 15 años de labor como enfermera profesional. Durante varios años trabajó en entidades sin ánimo de lucro en diferentes secciones en su país y fundó una ONG, que atiende hasta el día de hoy habitantes de la calle. Por dos años se desempeñó en trabajos administrativos en la Embajada de Israel en Alemania. Vivió y trabajó un año en Ginosar / Galilea / Israel (voluntariado social). Desde 1999 vive en Bogotá / Colombia, donde inicialmente dictó clases particulares de alemán e inglés (1999 - 2000) y luego quiso experimentar en el sector de los medios audiovisuales (2000 - 2002). En octubre 2002 decidió fundar la entidad sin ánimo de lucro Corporación Horizontes Colombianos de la cual es su Representante Legal y Gerente General. Tiene 37 años de experiencia como voluntaria y en la coordinación de voluntarios. Idiomas: alemán, español, inglés, hebreo y francés. Es una persona con alto sentido de responsabilidad social y compromiso con el logro de resultados. Exigente en el crecimiento personal con grandes fortalezas para trabajar en equipo y siempre tiene expectativas de un progreso permanente.
