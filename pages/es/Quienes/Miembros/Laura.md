---
identifier: laura
title: Laura Cristina Bahamón Villalba
img: Miembros/Laura.jpg
---

Nacida el 9 de marzo de 1989 en Neiva- Huila (Colombia). Licenciada en educación artística y cultura y cursando la especialización en infancias y juventudes. Vinculada al trabajo educativo con niños, niñas y jóvenes de las comunidades indígenas Nasa en el departamento del Huila y Cauca, también ha desarrollado prácticas de arte visual y corporal con niños y niñas con necesidades educativas especiales, con interés en el desarrollo de prácticas artísticas y culturales para la transformación social. Miembro y voluntaria de la Corporación Horizontes Colombianos desde el año 2018.
