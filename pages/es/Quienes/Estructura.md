---
date: 2019-01-05
identifier: estructura
permalink: /{{ parent | slug }}/estructura.html
title: Estructura Jurídica
---
**ESTATUTOS CORPORACIÓN HORIZONTES COLOMBIANOS**

**CAPITULO I Nombre, Domicilio, Objeto Social y Duración.**

**ARTICULO 1 NOMBRE DE LA ENTIDAD**. La entidad que por medio de estos estatutos se reglamenta, es una entidad sin ánimo de lucro, que se constituye como una Corporación y se denomina **CORPORACIÓN HORIZONTES COLOMBIANOS** y puede también actuar bajo la sigla de C. H. C.

**ARTICULO 2 DOMICILIO**. La entidad que se constituye tiene su domicilio en la ciudad de Bogotá D. C. / Colombia – Suramérica, su sede principal se encuentra en la CR 6 B Este No 88 G – 18 Sur.

Teléfonos: 310 254 5321 – 312 604 9037, pudiendo abrir oficinas fuera de Bogotá D.C. y en otros países del mundo.

**ARTICULO 3 DURACIÓN**. La entidad que se constituye tendrá una duración de sesenta años (60) contados a partir del 17 de Octubre del año 2002.

**ARTICULO 4  OBJETO SOCIAL.** Obtener el apoyo financiero e institucional de personas naturales y jurídicas enfocado a mejorar condiciones de educación, sanidad, bienestar social, así como a la  visibilización, promoción y difusión de las prácticas, saberes y tradiciones de comunidades indígenas, afro, campesinas y sus  diversas manifestaciones propias.

Promover iniciativas, programas o proyectos de medicina tradicional holística, con un enfoque ínter-cultural, la educación ambiental, investigación comunitaria, investigación-creación, turismo alternativo, convivencia y educación para la paz, acompañada de actividades que persigan mejorar la calidad de vida y convivencia.

Promover acciones para la paz, convivencia,  medio ambiente, saberes tradicionales y derechos humanos,  así mismo, la promoción del arte y la ritualidad como estrategias de sensibilización social entre las comunidades con un enfoque intercultural y vivencial.

Facilitar el intercambio cultural con personas de diferentes países que manifiesten interés en compartircon comunidades ancestrales y campesinasde Colombia y Latinoamérica.


[Descargar documento completo en formato pdf](/static/estatutos_pdf_sp_ak.pdf)
