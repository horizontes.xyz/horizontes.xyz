---
date: 2019-01-04
identifier: organograma
permalink: /{{ parent | slug }}/organograma.html
title: Organograma
layout: layouts/chart.njk
---

## Asamblea General

<pre class="mermaid">
  graph TD

  A[Asamblea General] --- B1[Fiscal]
  A --- B2[Junta de <br/> Asociados]
  B2 --- C1[Representante <br/> Legal]
  C1 --- D1[Grupo Consultor <br/> Y Convenios]
  C1 --- D2[Representación <br/> Indígena]
  D2 --- E1[Representante <br/> en Alemania]
  E1 --- F1[Suplente - <br/> Subgerente]
  B2 --- C2[Comunicadora <br/> Intercultural]
  C2 --- D3[Gestora Cultura]
  D3 --- E2[Asesor de Proyectos]
  E2 --- F2[Coordinador Área <br/> Ambiental]
  F2 --- G2[Asesor Jurídico]

  classDef someclass fill:#f96;
  class A someclass;
</pre>

## Grupo Consultor Y Convenios

<pre class="mermaid">
  graph LR
  A[Grupo Consultor <br/> Y Convenios]
  A --- B1[Pontificia Universiad <br/> Javeriana]
  B1 --- C1[Depto. de Ingenieria Civil]
  B1 --- C2[Depto. de Contaduria]
  B1 --- C3[Depto. de Administracion de Empresas]
  B1 --- C4[Fac. de Estudios Ambientales y Rurales]
  B1 --- C5[Fac. de Tecnologia]
  B1 --- C6[Fac. de Psicologiac]  
  B1 --- C7[Depto. de Lenguas Modernas]  
  B1 --- C8[Fac. de Enfermeria]
  B1 --- C9[Fac. de Derecho]
  A --- B2[Politécnico Grancolombiano]
  B2 --- C10[Fac. de Medios Audiovisuales]
  B2 --- C11[Fac. de Derecho]
  A --- B3[Otras Universidades]
  B3 --- C12[Univ. Jorge Tadeo Lozano]
  B3 --- C13[Univ. Nacional de Colombia Sede Amazonas]
  B3 --- C14[Univ. Pedagógica Bogotá]
  B3 --- C15[Univ. Nacional de Colombia Sede Medellin]
  B3 --- C16[Univ. Nacional Sede Palmira]
  A --- B4[Redes Voluntariados]
  B4 --- C17[Alianza Social Uniandinos]
  B4 --- C18[ONU Voluntariado]
  B4 --- C19[FONDACIO]
  A --- B5[Horizontes e. V- Alemania]
  A --- B6[ACITAM]
  B6 --- C20[Asociación de Cabildos Indigenas del Trapecio Amazónico]
  A --- B7[ONIC]
  B7 --- C21[Organización Nacional Indigena de Colombia]
  A --- B8[Otras Alianzas]
  B8 --- C22[Parroquia Maximilian Kolbe]
  B8 --- C23[EVT - Alemania]
  B8 --- C24[Tu Patrocinnio]
  B8 --- C25[Aid Connect]
  B8 --- C26[We care Charity Label]
  B8 --- C27[Parroquia Frohnleichnam]

  classDef someclass fill:#f96;
  class A someclass;
  class B1 someclass;
  class B2 someclass;
  class B3 someclass;
  class B4 someclass;
  class B5 someclass;
  class B6 someclass;
  class B7 someclass;
  class B8 someclass;
</pre>
