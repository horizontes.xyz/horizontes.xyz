---
date: 2019-01-03
identifier: objetivos
permalink: /{{ parent | slug }}/objetivos.html
title: Objetivos
img: objectives.jpg
---

Nuestro trabajo se encuentra alineado con los Objetivos del Milenio, entre ellos el de erradicar la pobreza, lograr educación básica, reducir la mortalidad infantil, promover la equidad de género, respetando la cultura y tradiciones de los pueblos originarios.
A través de nuestros proyectos buscamos:

 - Crear mayores y mejores oportunidades para la población.
 - Promover y facilitar el acceso a la educación superior, sin distinción de género.
 - Crear mecanismos pacíficos de defensa para las comunidades, que se encuentren en medio del conflicto armado.
 - Crear mayor autonomía e inclusión.
 - Reforzar el orgullo originario.
 - Mejorar las oportunidades de vida para los niños.
 - Crear una atmósfera de positivismo y motivación.
 - Aumentar los ingresos e incrementar los niveles de vida.
 - Reforzar y fortalecer las tradiciones, valores e identidad originaria.
 - Mejorar las condiciones de salud e higiene de la población.
 - Crear ambientes más sanos y productivos.
 - Mejorar las condiciones y estabilidad de la comunidad.
 - Generar nuevos y mejores conocimientos, haciendo uso de las nuevas tecnologías.
