---
identifier: Sanitation
layout: layouts/gallery.njk
title: Saneamiento Básico
img: Sanitation/12.jpg
pics: 14
---

El “proyecto piloto para la implementación de metodologías y estrategias de saneamiento básico en comunidades indígenas de Colombia, caso: Comunidad Tikuna Nazareth – Amazonas”, era una iniciativa de la CHC en alianza con el Dpto. de Ingeniería Civil de la Pontificia Universidad Javeriana de Bogotá, que propendió por mejorar las condiciones sanitarias de la comunidad mediante el mejoramiento del suministro de agua potable y alcantarillado y la gestión de un adecuado manejo de los residuos sólidos.
Los estudios y diseños fueron documentados y avalados por la Asociación de Cabildos Indígenas del Trapecio Amazónico (ACITAM), el Ministerio del Medio Ambiente, Corpoamazonía, la Gobernación del Amazonas y la Alcaldía de Leticia. La comunidad se encargó de la gestión en España para la realización de este proyecto.
