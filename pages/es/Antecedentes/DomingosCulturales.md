---
identifier: DomingosCulturales
layout: layouts/gallery.njk
title: Domingos culturales
img: DomingosCulturales.jpg
pics: 3
---

El proyecto se puede denominar como unos de los primeros que puso en marcha la CHC con el fin de acercar el arte a las clases populares, dando la oportunidad de contemplarlo, analizarlo, discutirlo y practicarlo en un marco didáctico en comunicación directa con algunos artistas y su obra.

Hemos buscado  a través de estos contactos con el arte una transformación en las personas, para lograr un equilibrio dentro de cada cual, un mejor entendimiento del mundo y de la vida, llevándoles a una actitud positiva.

Enfocamos nuestro trabajo en el Consuelo Sur, un barrio popular al extremo sur de la ciudad en conjunto con la Junta de Acción Comunal para determinar los sitios y las formas en que se convocó a la comunidad, alrededor de las prácticas artísticas (música, teatro, danza contemporánea, literatura, poesía, documentales sobre diferentes temas).

La CHC consiguió donaciones de refrigerios en particular para las niñas y niños y pequeños regalos útiles.

Involucramos en nuestras visitas otro proyecto que hemos diseñado “Basuras no basuras” y animamos a la gente a reutilizar y reciclar, generando experiencias de creación artística con la supuesta basura, de allí salieron piezas de arte, bajo la dirección de artistas plásticos.   

En un platón de un camión y con megáfono recorrimos las calles para convocar a la población y en la cancha de fútbol del barrio encontramos el sitio más adecuado para trabajar en jornadas largas y con excelente participación de los habitantes, lo que permitió compartir con la comunidad generando espacios de convivencia pacífica y artística.  
