---
identifier: Iburi
layout: layouts/gallery.njk
title: IBURI – Nuestra mirada indígena
img: IBURI_foto_1.jpg
pics: 14
---

Este era un Canal de Televisión en circuito cerrado con programación especialmente enfocada a la comunidad de Nazareth – Amazonas y las comunidades Tikunas a su alrededor. El objetivo era fortalecer la cultura Tikuna por medio de la presentación de audiovisuales, de temas importantes sobre su cultura, a la vez de rescatar y conservar las prácticas ancestrales como memoria audiovisual para las futuras generaciones.

La programación mensual se emitió en lengua Tikuna y español, donde se programaba un noticiero de alto contenido regional, documentales, cortometrajes, video clips, películas, campañas educativas y de recuperación de valores.  Todo el contenido era elaborado en conjunto con la comunidad. Se suministraron equipos audiovisuales y se nombró la directora Luz Dary Mojica para su buen funcionamiento a futuro, donde los indígenas harán sus propios programas, incluyendo su emisión dentro de la comunidad y las demás comunidades Tikunas del Amazonas.

En el marco del proyecto se realizó la película “El Origen del Pueblo Tikuna”.
IBURI era una realización de la CHC con el apoyo del Politécnico Grancolombiano.
