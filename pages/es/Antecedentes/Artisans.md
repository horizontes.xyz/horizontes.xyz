---
identifier: Artesanos
layout: layouts/gallery.njk
title: Cooperativa de Artesanos del Amazonas
img: artesanos_foto_1.jpg
pics: 12
---

Este proyecto propendió generar un desarrollo social integral de las Comunidades Originarias del Trapecio Amazónico, generando espacios productivos y cultivando las habilidades de asociación y cooperativismo entre los artesanos, buscando así el rescate y la promoción del trabajo ancestral de la región.

A su vez propendió por abrir canales de distribución y venta de los productos, mediante alianzas y puntos propios, bajo el concepto de Comercio Justo, para mejorar así los ingresos y las condiciones de vida de los 1.500 artesanos en las 18 comunidades, vinculándolos al proyecto.

Esta iniciativa fue planeada con el apoyo de la Asociación de los Cabildos Indígenas del Trapecio Amazónico – ACITAM, el SENA sede Leticia, la asesoría en formación empresarial de la Fundación Julio Mario Santo Domingo y la Universidad Distrital Francisco José de Caldas en el ámbito ambiental.
