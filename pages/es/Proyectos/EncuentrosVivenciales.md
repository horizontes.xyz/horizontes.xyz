---
identifier: EncuentrosVivenciales
title: Encuentros Vivenciales
img: encuentrosvivenciales_foto_1.png
layout: layouts/gallery.njk
pics: 8
---

El proyecto fue creado luego de profundas reflexiones acerca del compartir, interconectarse, reconciliarse y auto conocerse.
Alejados de la natura, en ciudades ruidosas, aire contaminado y rutinas estresantes, nos condenamos a perder el sentir de lo esencial de la vida y hacernos prisioneros de nuestros propios inventos.
Los encuentros vivenciales permiten a los participantes compartir saberes y prácticas en territorios naturales, junto a comunidades que trabajan en la preservación de estos mágicos lugares, disfrutando y aprovechando el silencio para una amplia, profunda e íntima introspección acerca el mundo y la vida.
Dirigido a empresas públicas y privadas, instituciones educativas y al público en general, con la necesidad de distensionarse y de expandir la noción del tiempo.
Caminatas a las montañas, la laguna, alimentarse sanamente y compartir palabra y música alrededor del fuego nos llevarán a la meditación y por ende a un bienestar corporal y espiritual, para retornar fortalecidos y armonizados a nuestros hogares.
