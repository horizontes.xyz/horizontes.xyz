---
identifier: Intercambioetnico
permalink: /{{ parent | slug }}/intercambioetnico.html
title: Del lejano amanecer al despertar del nuevo continente
img: intercambioetnico_foto_1.jpg
layout: layouts/gallery.njk
pics: 12
---

Propuesta de Investigación - Documental: puntos de convergencia entre diferentes grupos étnicos de Colombia y China.

El proyecto de intercambio étnico-cultural está enfocado en la investigación, documentación  y acercamiento de los diferentes puntos de convergencia, entre los grupos étnicos de Colombia y China, con los cuales  la Corporación realiza trabajo conjunto.  Esta iniciativa busca  estrechar los lazos de hermandad e intercambio entre los pueblos ancestrales de los dos continentes, construyendo colectivamente esfuerzos que por medio de la cultura  nos permita entender las similitudes y diferencias etnográficas, pero también las formas de pensamiento colectivo que permitan ayudar en la labor de vivir en armonía con la naturaleza y los demás seres, habitantes de la tierra.
