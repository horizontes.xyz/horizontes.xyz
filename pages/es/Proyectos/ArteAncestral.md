---
identifier: ArteAncestral
layout: layouts/gallery.njk
title: Arte Ancestral
img: ArteAncestral/10.jpg
notopimg: true
pics: 10
---

<div class="fr pa-l">
<img width="256px" src="/static/tienda_en_linea.jpg"/>
<h3>Tienda en línea</h3>
</div>

Proyecto para la promoción de las artesanías  de diferentes pueblos indígenas

El proyecto nace  de la propuesta adelantada por las comunidades Tikuna y la CHC  “Cooperativa de Artesanos del Trapecio Amazónico” la cual buscaba integrar las Comunidades Indígenas en espacios productivos, cultivando las habilidades de asociación y cooperativismo, buscando el rescate, la conservación y promoción del trabajo ancestral de las comunidades.

Con la intención de retomar y ampliar este proyecto a las demás comunidades con las que la CHC trabaja en cooperación, se pretenden abrir canales de distribución y de venta para las artesanías, mediante alianzas y bajo el concepto de Comercio Justo (Fair Trade), mejorar así los ingresos y las condiciones de vida de artesanos de las  diferentes comunidades vinculadas al proyecto.

Este proyecto se realiza con el apoyo de la Asociación de los Cabildos Indígenas correspondientes y entidades como la ACITAM,  la Universidad Distrital Francisco José de Caldas y el Cabildo Estudiantil de la Universidad Surcolombiana.

En la actualidad la CHC busca fortalecer y vincular diferentes artesanos tradicionales a una plataforma interactiva donde puedan exponer sus trabajos, buscando aliados dentro del sistema del comercio justo.
