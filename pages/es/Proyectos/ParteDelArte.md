---
identifier: ParteDelArte
permalink: /{{ parent | slug }}/parte-del-arte.html
title: Parte del Arte – Galería
img: parte_del_arte_foto_2.jpg
notopimg: true
layout: layouts/gallery.njk
pics: 9
---

<img src="/static/parte_del_arte.jpg"/>
La Corporación Horizontes Colombianos CHC inició en octubre de 2003 su labor cultural en el espacio “Parte del Arte – Galería”, ubicada en la sede por ese entonces de la CHC.

Este proyecto fue creado con la intensión de establecer un intercambio entre artistas colombianos y alemanes. Contó durante 4 años con un espacio físico, donde se realizaron múltiples exposiciones colectivas e individuales de artistas colombianos y alemanes como: Carlos Rojas, Edgar Negret, Darío Ortiz, Doris Hinzen – Roehrig, Umberto Giangrandi, Mario Salcedo, Jim Amaral, Gladys Ortiz entre muchos otros y otras artistas.

Este espacio fue abierto a exposiciones de artistas en condición de discapacidad como Aydée Ramírez, a excelentes artistas poco conocidos y así mismo a exposiciones de tesis de grado de estudiantes de la Pontificia Universidad Javeriana.

La galería era conocida por un movimiento constante de intercambio y por sus actividades artísticas y sociales.

Exposiciones que realizó la CHC:
“Los niños también cuentan”. Exposición colectiva, organizada por la CHC y UNICEF Colombia.

“Identidad – Realidad”. Exposición colectiva. Un homenaje al artista Carlos Rojas, con la participación de Gladys Ortiz, Mario Salcedo y obras del maestro Rojas.

“Concierto de Minotauros”. Exposición individual de Nubia Medina.

“Llegada en mar inquieto”. Exposición del artista alemán Uwe Anders.

“Contrastes visuales”. Exposición individual de la artista Mari Rincón de Medellín.

“El silencio de la luz”. Exposición individual del artista Germán Méndez.

“Usurpados”. Exposición individual de carácter social de la pintora con la boca Aydée Ramírez

“Sky Line”. Exposición de la artista alemana Doris Hinzen Roehrig de Berlín.

“Mujeres en el Arte”. Exposición colectiva de 4 mujeres vinculadas a la CHC en el Club ECOPETROL.

“Visión concreta”. Exposición individual de la artista Raquel Ramírez de Bucaramanga.

“Una mirada desde el corazón”. Exposición individual del artista Jhon Jairo Restrepo del Quindío para un público invidente y vidente.

“Que llevamos dentro”. Exposición de la artista alemana Petra Jung, acompañada de una instalación audio – visual de la artista colombiana Antonia García.

“Vista interior – Vista exterior”. Exposición colectiva en Berlín – Alemania con la participación de la artista colombiana Mari Rincón de Medellín, vinculada a la CHC.

“Ángeles en el Arte”. Exposición colectiva con la participación de artistas colombianos y alemanes.
Exposición de tres trabajos de tesis de estudiantes de la Carrera de Bellas Artes de la Pontificia Universidad Javeriana.

“Una mirada desde el corazón”. Exposición itinerante del artista Jhon Jairo Restrepo, organizada por la CHC en Cali / Valle.

“Grabve – Terrible”. Exposición colectiva de tres artistas colombianos, mostrando sus trabajos de grabado.

“Memorias de la ciudad”. Exposición de tres artistas estudiantes; trabajos de tesis de la Carrera de Bellas Artes de la Pontificia Universidad Javeriana.

“Visión de Panorama”. Exposición colectiva de artistas colombianos, alemanes y nordamericanos.

“Experiencias del baño…” Exposición individual de una artista con su trabajo de grado de la Carrera de Bellas Artes de la Pontificia Universidad Javeriana.

“Herencia ancestral” – Gran exposición de arte indígena, acompañada de fotografías, videos y sonido del Amazonas, con la participación de estudiantes de diferentes universidades de Bogotá.

“Buscando Huellas”. Exposición del escultor colombiano Oscar Pulido; vive y trabaja en Colonia / Alemania y expuso por primera vez en Colombia.

“Continuidad de la naturaleza a través del desarrollo urbano”. Exposición colectiva del artista Kevin Nazar de Honduras y Maria Andrea Umaña de Colombia.

“Insinuaciones naturales”. Exposición individual de la artista Angélica Chaparro; egresada de la Universidad Nacional.

“Explosión cósmica”. Exposición individual del artista César López de su trabajo de la Nueva Acuarela, acompañada de un video clip de la CHC “Cinco variaciones y un réquiem”.
Cierre del espacio “Parte del Arte – Galería” en el 2007 por falta de apoyos financieros.

La CHC ha realizado una gran labor con este proyecto, incluyendo todos los estratos, como de artistas y espectadores, despertando el gusto por una de las bellas artes, que son las artes pláticas, que va en contra de la violencia, que vive el país.

Hemos podido recaudar fondos para la CHC, así mismo apoyando artistas, que viven cada vez más una situación complicada y además complacer las personas, que han visitado este espacio.

Por medio de “Parte del Arte – Galería” muchas personas se enteraron de la gestión social de la Corporación Horizontes Colombianos.

Se tiene la intención retomar el proyecto para su funcionamiento vía online.

<video controls>
 <source src="/static/parte_del_arte.mp4" type="video/mp4">
</video>
