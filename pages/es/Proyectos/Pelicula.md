---
identifier: pelicula
layout: layouts/item.njk
permalink: /{{ parent | slug }}/el-origen-del-pueblo-tikuna.html
navtitle: 'Película: “El Origen del Pueblo Tikuna”'
img: poster_film.jpg
---

# “El Origen del Pueblo Tikuna” - El Rescate de un mito y una lengua

Buscando visibilizar a las comunidades indígenas de Colombia, la CHC realizó, junto con la comunidad, su primer largometraje etnográfico “El Origen del Pueblo Tikuna”, que fue grabado en el 2008, galardonado como Mejor Opera Prima en Los Premios de la Gran Manzana al Cine Latinoamericano 2009 - Nueva York, y Premio TIC Ministerio de Cultura de Colombia 2008.
Esta es una iniciativa de la comunidad indígena de Nazareth, que surge como proyecto del Canal IBURI (canal de televisión comunitario).
La película narra según las creencias ancestrales, el origen de esta etnia que se extiende por Brasil, Colombia y Perú, logrando así una memoria audiovisual de la tradición revelada por medio oral durante siglos.
Esta primera película – argumental, filmada con la participación de los Indígenas de Nazareth – Amazonas, es la primera producción en Colombia, grabada en una lengua diferente al español y con actores Indígenas y más del 60 % del personal técnico, igualmente Indígenas, contó con un Premio del Ministerio de Cultura en la Modalidad de las TIC con inclusión social, el apoyo del Politécnico Grancolombiano y la CHC. Fue subtitulada en español, alemán, inglés, francés, portugués e italiano. Vale la pena destacar, que la película se ha convertido en un elemento visibilizador de las etnias del Amazonas.

 - [TRAILER](http://www.youtube.com/watch?v=zYQb4f-CaGU)
 - [PELICULA](https://www.youtube.com/watch?v=cN-SG6nASLQ&pbjreload=10)

DETRÁS DE CAMARAS
 - [Investigación y desarrollo de la película](https://www.youtube.com/watch?v=y85Z83XJYLE)
 - [Preproducción](https://www.youtube.com/watch?v=gbkFmHw5WXo)
 - [Producción](https://www.youtube.com/watch?v=z64X-gx5oj4)
 - [Postproducción](http://www.youtube.com/watch?v=KiueXm8wX84)


<div class="fr pa-l">
<img src="/static/Pelicula/1.jpg" />
</div>


**Del Amazonas a La Gran Manzana**
 De una minoría para el mundo
 “El Origen del Pueblo Tikuna”


<div class="fl xpa-r mw-5">
  <img src="/static/Pelicula/2.jpg"/>
</div>


El jurado de los “Premios La Gran Manzana del Cine Latinoamericano” otorgó, el 17 de septiembre de 2009, el trofeo en la categoría de Mejor
Opera Prima al largometraje “El Origen del Pueblo Tikuna” que narra el mito de la creación según los pueblos indígenas del Trapecio Amazónico.

<div class="fr pa mw-5">
  <img src="/static/Pelicula/3.jpg"/>
</div>

Esta película, que dio a conocer la situación de una minoría étnica de los países del Amazonas y que tuvo como actores a nativos de dichas etnias, fue rotulada como sagrada por los ancianos del pueblo y ahora se podrá apreciar en muchos escenarios del mundo que valoran estas manifestaciones culturales y artísticas.
Para la CHC es un orgullo compartir con el público en general, con quienes apoyan su trabajo y con quienes apenas los oyen nombrar, la alegría que palpita en el corazón de sus protagonistas, miembros de la comunidad indígena, de su director (Gustavo de la Hoz) y de su productora (Dorothea Wolf Nurnberg) quien es también la fundadora y representante legal de la Corporación.
Felicitaciones para los actores indígenas que interpretaron su historia para aquellos que no la conocíamos.

**La CHC realizó presentaciones en los siguientes espacios universitarios y culturales:**
**Lanzamiento del largometraje “El Origen del Pueblo Tikuna” en la comunidad Tikuna Nazareth - Amazonas**

<div class="fl pa-r">
<img src="/static/Pelicula/4.jpg"/>
</div>

A principio de febrero 2009 fue presentada en exclusiva la película en el resguardo Nazareth- Amazonas, este fue un acto lleno de magia, alegría y optimismo, al cual asistieron los protagonistas, los técnicos, la comunidad en general y cuatro  profesores de la Universidad Javeriana de Bogotá, invitados por la Corporación Horizontes Colombianos. Esta es la primera vez que los Tikuna ven en la pantalla grande uno de sus mitos sagrados.


**Exitosa Première del Largometraje “EL ORIGEN DEL PUEBLO TIKUNA” en el Politécnico Grancolombiano de Bogotá, con la presencia de personajes de la Cultura y de algunos protagonistas de la obra cinematográfica.**

<div class="fr pa-l mw-15">
<img src="/static/Pelicula/6.jpg"/>
</div>

El largometraje “El Origen del Pueblo Tikuna”, que cuenta ya con un premio del Ministerio de Cultura fue presentado el 28 de abril de 2009 en las instalaciones del Politécnico Grancolombiano con la presencia del equipo técnico de Bogotá, su director Gustavo de la Hoz, la productora  Dorothea Wolf-Nurnberg y sus protagonistas, Juvencio Pereira Manduca y Yenika Mojica Pereira, de la etnia Tikuna.
<div class="fl pa-r mw-10 ">
<img src="/static/Pelicula/7.jpg"/>
</div>

Dr. Pablo Michelsen Niño, Rector del Politécnico, se refirió a la producción como una muestra del trabajo de proyección social de la importante institución educativa y como reflejo de lo que han venido haciendo en cinematografía con aplicaciones de la actividad académica en el entorno nacional.

<div class="fr pa-l mw-10">
<img src="/static/Pelicula/8.jpg" />
</div>

Por su parte, Carlos Augusto García, Decano de la Facultad de Ciencias de la Comunicación y Artes  expresó su satisfacción con este trabajo que, en sus palabras, renueva y reinventa la pedagogía con un ejercicio de creación y presencia de la memoria colectiva con el mito de la creación del pueblo Tikuna, del Amazonas.

<div class="fl pa-r mw-8">
<img src="/static/Pelicula/9.jpg" />
</div>

El Ministerio de Cultura, a través de Germán Franco, destacó cómo este largometraje muestra la diversidad cultural del país y reconoció la inclusión en este gran esfuerzo cinematográfico de aquellos que durante mucho tiempo han sido excluidos.
Destacada participación tuvo también Silsa Arias en representación de la Organización Nacional Indígena de Colombia - ONIC, quien manifestó la importancia de la apertura de la academia a aquellos que han sido invisibles en nuestra sociedad para permitirles hacer su aporte al país desde la mirada de su pueblo. Nos dejó además como enseñanza que el mito es una realidad para los pueblos indígenas.
<div class="fr pa-l w-10">
<img src="/static/Pelicula/10.jpg" />
</div>
Fue común denominador el agradecimiento a la comunidad Tikuna por abrir sus puertas al equipo de trabajo que liderado por la Corporación Horizontes Colombianos, interactuó con sus miembros para llegar a este largometraje que dio mucho de qué hablar en los diferentes escenarios donde se exhiba.
 El 5 de mayo de 2009 fue presentada la película, en dos funciones, a la comunidad universitaria en general con sus respectivos cine foros.

**La Fundación Zuá disfruta “El Origen del Pueblo Tikuna”**
<div class="fl pa-r w-15">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/11.jpg'">
<img src="/static/Pelicula/11.jpg" />
</a>
</div>
 El pasado 23 de mayo de 2009 el film fue proyectada en la Fundación Zuá en Sassaima – Cundinamarca, con la participación de los estudiantes Indígenas de CHC, que en parte protagonizaron el film, compartiendo esta experiencia con los beneficiados de la Fundación Zuá, quienes son personas marginadas, habitantes de Patio Bonito y que a través de la Fundación, tienen la posibilidad de estudiar una carrera técnica o profesional. Este intercambio dejó una enseñanza para ambas partes. A este encuentro asistieron igualmente los dirigentes de las dos entidades sin ánimo de lucro.

**El Origen en la Universidad Nacional de Colombia – Sede Medellín**
<div class="fr pa-l w-10">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/12.jpg'">
<img src="/static/Pelicula/12.jpg" />
</a>
</div>
 El 1 de agosto de 2009 se presentó la película, a un pequeño grupo de estudiantes, especialmente Indígenas de diferentes etnias y regiones del país. Una vez terminada la proyección, se desarrolló un diálogo muy interesante acerca de la realización y su propósito.

**“El Origen del Pueblo Tikuna” en la apertura del Simposio del natalicio de Charles Darwin La Universidad Tadeo Lozano hace homenaje al hombre de ciencia**

<div class="fl pa-r w-10">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/13.jpg'">
    <img src="/static/Pelicula/13.jpg" />
  </a>
</div>

 El 18 de agosto de 2009  en el Aula Máxima de la Universidad Tadeo Lozano se presentó la película El Origen en el marco del Simposio organizado por el área de Humanidades en la cátedra Darwin, Biología Marina y Centro de Arte y Cultura.
 Se realizó un foro en el cual estuvo como moderador Mauricio Laurence y contó entre sus participantes con Gustavo de la Hoz de la CHC, Director de la película y Yenica Mojica Pereira, protagonista Tikuna. Cabe destacar la masiva participación de los estudiantes y su interés por conocer el tema más a fondo.

**El “Origen del Pueblo Tikuna” en Espacio Alterno de Uniandinos –
 Egresados de la U. Los Andes**
 <div class="fr pa-l w-10">
 <img src="/static/Pelicula/14.jpg" />
 </div>
 El 8 de septiembre de 2009 fue presentada la película de la Corporación Horizontes Colombianos “El Origen del Pueblo Tikuna” en Espacio Alterno para rendir homenaje a la cultura Tikuna, organizado por la Alianza Social y la Subdirección Cultural de Uniandinos.
 Para esta presentación fueron invitados Miguel Dionisio Ramos, Juvencio Pereira Manduca, Yenica Mojica Pereira y Rosenaida Dionisio Quirino, protagonistas del film, además la Productora Ejecutiva Dorothea Wolf – Nurnberg y el Director Gustavo De La Hoz, quiénes participaron luego de la proyección en el cine foro. Como acompañamiento a la proyección de la película se organizó una muestra artesanal.

**“El Origen del Pueblo Tikuna” para estudiantes de Cine y Televisión**   
<div class="fl pa-r w-15">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/15.jpg'">
<img src="/static/Pelicula/15.jpg" />
</a>
</div>
 El 6 de octubre de 2009, los estudiantes de Cine y Televisión, disfrutaron la proyección de la película en las instalaciones de la Fundación Lumière. Esta producción cinematográfica causó un gran impacto en los futuros cineastas, entendiendo la nueva propuesta en el panorama del Cine Colombiano.
 Se ofreció, como costumbre y necesidad al público el cine foro, despejando muchas inquietudes con los indígenas asistentes y el Director y la Productora Ejecutiva de la misma.

**Confrontación de la película “El Origen del Pueblo Tikuna” con Antropólogos, Sociólogos e Historiadores**
<div class="fr pa-l w-10">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/16.jpg'">
<img src="/static/Pelicula/16.jpg" />
</a>
</div>
 El 8 de octubre de 2009 se presentó en la Pontificia Universidad Javeriana, el largometraje para estudiantes de las Carreras Antropología, Sociología e Historia.
 Por la limitación del tiempo, no se pudo realizar un cine foro tan esperado por la CHC y los Indígenas participantes, pero se realizó un pequeño conversatorio.


**“El Origen del Pueblo Tikuna” y el Medio Ambiente**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/17.jpg" />
</div>
 El 16 de octubre de 2009 fue presentada en la Universidad Distrital Francisco José de Caldas, la película para los estudiantes de las carreras, relacionadas con el Medio Ambiente.
 “Después de los 517 años del (descubrimiento de América), nuestro presente, nuestra violencia, nuestro olvido, nuestros comportamientos, son el consecuencia  de la aculturación en América. El sufrimiento de nuestros pueblos y su actual agonía, recuerdan que aún no termina la persecución histórica de nuestras raíces.
 Rescatar la importancia de nuestras raíces indígenas es de vital compromiso con nuestro pasado vivo y nuestro futuro incierto…” Rafael – Estudiante de la Carrera Administración Ambiental.  

**Segunda presentación en la Pontificia Universidad Javeriana**
<div class="fr pa-l w-15">
<a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/18.jpg'">
<img src="/static/Pelicula/18.jpg" />
</a>
</div>
 Esta presentación del “Origen del Pueblo Tikuna”, el 19 de octubre de 2009, estuvo abierta al público y se realizó en el Auditorio Marino Troncoso de la Pontificia Universidad Javeriana.
 Se encontraron estudiantes de diferentes universidades de Bogotá, para contemplar el film y compartir opiniones en el cineforo. Esta proyección estuvo acompañada por los Indígenas y representantes de la CHC. Así mismo se realizó una muestra del arte ancestral Tikuna.


**“El Origen del Pueblo Tikuna” en el V Festival Internacional de Cine de Pasto**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/19.jpg" />
</div>
 En el mes de agosto 2009 se realizó en Pasto el Encuentro de los Chamanes, Mamos y Taitas (1er Encuentro Internacional de Culturas Andinas y del Pacífico) como un intercambio de saberes y  desarrollo de conferencias sobre medicina tradicional sin dejar de lado las ceremonias, los rituales, los cantos y las danzas, entre otras manifestaciones artísticas.
 Los países participantes en esta oportunidad en que Colombia era el anfitrión fueron: Chile, Venezuela, Argentina, Bolivia, Perú y Ecuador.
 Al mismo tiempo se realizaba el V Festival Internacional de Cine de Pasto que contó con la participación de profesionales del Cine y la Televisión, así como con la asistencia de conferencistas nacionales e internacionales.

 <div class="fr pa-l w-15">
 <img src="/static/Pelicula/20.jpg" />
 </div>
 La Corporación Horizontes Colombianos representada por Dorothea Wolf – Nurnberg quien se desempeña como Representante Legal fue invitada especial con “El Origen del Pueblo Tikuna”,  como película fuera de concurso.
 Dorothea, quien es  además, la  Productora Ejecutiva de la Película estuvo como invitada en el Panel que se organizó en la Universidad de Nariño con los documentalistas Martha Rodríguez, de Colombia, Juan Martín Cueva Director del Festival de Cine documental “Cero Latitud” de Ecuador, Humberto Mancilla, Director del Festival de Cine de Derechos Humanos de Bolivia y el Gobernador del Departamento de Nariño, Antonio Navarro Wolff. Se hizo énfasis en la memoria y las producciones audiovisuales.

**Invitación al 2º Festival Internacional de Cine y Video Alternativo y Comunitario “Ojo al Sancocho” en Bogotá D.C.**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/21.jpg" />
</div>
 El 22 de septiembre de 2009 la Corporación Horizontes Colombianos fue invitada con la película “El Origen del Pueblo Tikuna”, a la segunda edición del Festival Internacional de Cine y Video Alternativo y Comunitario “Ojo al Sancocho” en la localidad de Ciudad Bolívar en Bogotá. En esta tarde fue presentado el Trailer de la película, acompañado de un conversatorio, con la presencia de delegados de Pueblos Indígenas y Afrodescendientes, con representación de Bolivia y Colombia. Así mismo participaron en esta mesa de dialogo la Representante Legal de la CHC y Productora Ejecutiva de la película y el Director Gustavo De La Hoz.

**1º Muestra de Cine y Video Indígena en Colombia “Daupará” – Para ver más allá**
<div class="fr pa-l w-15">
<img src="/static/Pelicula/22.jpg" />
</div>
 Este proyecto forma parte de una propuesta común de las comunidades y Pueblos Indígenas a lo largo de toda América. Hoy más que nunca, cuando nuestra Madre Tierra agoniza lentamente, se unen en su defensa voces, imágenes, sentimientos, apuestas, y luchas de los Pueblos Indígenas. Desde sus propias ópticas y manos se  resembrarán en el imaginario colectivo colombiano a través de la Primera Muestra de Cine y Video Indígena en Colombia, cuya base será un mochilón  multicolor repleto de frutos audiovisuales realizados y repensados por miembros de pueblos y comunidades indígenas de nuestro país en los últimos años, bajo el impulso y sintonía de producciones ganadoras en el IX Festival Internacional de Cine y Video de los Pueblos Indígenas, desarrollado en Bolivia en el año 2008.
 La Corporación Horizontes Colombianos fue invitada con su película “El Origen del Pueblo Tikuna”  a participar en cuatro mesas de trabajo, donde se debatieron los siguientes temas: Experiencias en Comunicación Indígena, Intercambio de Saberes entre realizadores Indígenas y No – Indígenas, Políticas Públicas y Comunicación Étnica, Los medios y la Representación Étnica. El Origen fue presentada en la Universidad Nacional de Colombia en el marco del Festival.

**1º Festival de Cine y Video de San Agustín**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/23.jpg" />
</div>
 Este naciente evento de los medios audiovisuales se desarrolló del 13 al 16 de noviembre de 2009. Fueron cuatro días para apreciar el desarrollo del nuevo cine colombiano, haciendo énfasis en el cine y video etnográfico y social.
  El público asistente tuvo la oportunidad de dialogar con directores, guionistas y académicos durante los seminarios, talleres y conversatorios que se realizaron en el marco del Festival.
 “El Origen del Pueblo Tikuna” formó parte de la programación, junta a producciones audiovisuales de México y de Colombia.

 La película además fue presentada en espacios culturales y universitarios, así mismo en Festivales Cinematográficas en Bolivia, Alemania, Italia, Francia y España.   
