---
identifier: Investigadores
title: Investigadores en Etapa Escolar
img: investigadores_foto_1.jpg
---
El proyecto se diseña desde la iniciativa de apoyar estudiantes de grado décimo y once de la institución educativa José Reyes Pete Sede Instituto Técnico Etno- Ecológico “Cxamb Wala” (pueblo grande), ubicado en el resguardo Indígena de Vitoncó (Páez-Cauca), quienes desarrollan propuestas etno-ecológicas como requisito de grado. Allí se evidencia un gran interés en aportar a la pervivencia cultural desde el sentir de jóvenes indígenas; a partir de estas iniciativas se rescatan tradiciones y saberes propios, se investiga de acuerdo a la cosmovisión y creencias, con acompañamiento de las mayoras, mayores y líderes de la comunidad, afianzando la comunicación intergeneracional. De este modo se busca abrir camino para posicionar espacios de participación juvenil, construidos desde la autonomía cultural, donde hay un respeto profundo por la madre tierra, el territorio y los espíritus que lo habitan. Todas las acciones desarrolladas para cumplir los objetivos, priorizan el WËT WËT FIZENXI  o buen vivir, para habitar en armonía.

<video width="90%" controls>
 <source src="/static/Presentación_Deny.mp4" type="video/mp4">
 Su navegador no soporta la etiqueta de vídeo.
</video>

La CHC tuvo el gusto de apoyar el proceso de creación de la cartilla de recopilación de cuentos tradicionales: “Yeckatê´we´sx (yuwetx katxude ec)”, iniciativa realizada por la estudiante indígena nasa Denny Milena Cayuy para optar por el título de bachiller académica del colegio José Respetes.
El apoyo de esta iniciativa es un logro conjunto que tiene como fin el fortalecimiento de las tradiciones y los saberes propios, promoviendo la preservación de la lengua oral y escrita, haciendo llegar estos relatos principalmente a los niños, niñas y jóvenes de la comunidad indígena. Así se dan a conocer estos relatos con calidad editorial e imágenes realizadas por los propios niños de la comunidad, haciendo de este un proceso de la comunidad que se piensa a sí misma, desde sus intereses y necesidades como lo son la preservación de la cultura propia en medio del contexto de globalización que tiende a mercantilizar y homogenizar las culturas. Por esto es de suma importancia el fortalecimiento de la identidad propia mediante este tipo de iniciativas que apuntan a que los niños y jóvenes sean conocedores y multiplicadores orgullosos de su historia indígena.
Actualmente trabajamos en la consecución de recursos con el ánimo de realizar una versión impresa en pasta dura y con análisis en español de los cuentos, con el ánimo de difundir el trabajo en otros resguardos indígenas Nasa e instituciones educativas en los departamentos del Cauca y el Huila.
