---
identifier: Educacion
permalink: /{{ parent | slug }}/educación.html
title: La Educación – base para una vida digna
img: proy_edu_foto_1.jpg
notopimg: true
layout: layouts/page.njk
---


## Antecedentes

Mediante la formación de profesionales en las diferentes áreas del conocimiento, la Corporación Horizontes Colombianos propende hasta el día de hoy por empoderar a la población, brindando a jóvenes indígenas acceso a estudios universitarios en diferentes carreras, elegidas por ellas y ellos

![](/static/proy_edu_foto_1.jpg)

Dentro del programa han culminado su carrera superior 15 indígenas de diferentes etnias de Colombia, mujeres y hombres, con apoyo entero y parcial por parte de la CHC y cumplieron con su compromiso ante la Corporación, distribuyendo sus conocimientos requeridos a sus comunidades originarias y trasmitir sus experiencias durante su permanencia en la universidad y por ende en ciudades como Bogotá, Palmira y Medellín.

{% set path = identifier + '/Antecedentes' %}
{% set pics = 8 %}
{% include "components/gallery.njk" %}

## Actualidad

La educación, como derecho fundamental asegura en particular a minorías étnicas como son las comunidades indígenas y afrodescendientes, una participación activa en el desarrollo social, cultural del país. Marginados por siglos han sufrido violencias y desprecio de su cultura por la sociedad en general, debido a una inmensa indiferencia. Accediendo a estudios universitarios tienen los pueblos la oportunidad de darse a conocer y estar equitativamente al mismo nivel de oportunidades como sus compañeros y compañeras blanco mestizos, ejerciendo su capacidad intelectual y compartiendo su conocimiento ancestral.

![](/static/proy_edu_foto_2.jpg)

En la actualidad la CHC está apoyando a 5 estudiantes, mujeres y hombres del Pueblo Nasa del Cauca en su formación universitaria en diferentes carreras en la Universidad Surcolombiana en la ciudad de Neiva - Huila. Jóvenes líderes responsables que se encuentran en la tarea de brindar asesoría a partir de sus carreras, en las comunidades a las que pertenecen.
Nuestra meta para el 2020 es la de apoyar a 2 estudiantes más y dejar crecer paulatinamente el programa, con un mayor número de estudiantes vinculados.

{% set path = nil %}
{% set pics = 14 %}
{% include "components/gallery.njk" %}
