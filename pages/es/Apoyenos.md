---
date: 2019-01-06
identifier: Apoyenos
title: Apóyenos
layout: layouts/base.njk
permalink: /{{ title | slug}}.html
tags:
 - nav_es
---

# EL VALOR DE LA SOLIDARIDAD

<main class="flex jc-c">
<div class="pa-l-2 pa-r">

<iframe width="888" height="666" src="https://www.youtube.com/embed/LFeuJWNyCHU" title="CHC- Apoyenos - Suport us" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Elige la manera de apoyar:</h2>
<h3>Plan Padrino - Pa Chawene “Una mano para mi hermano”</h3>

<a href="/static/Plan-Padrino_pa-chawene_es.pdf">
Presentación
</a>
<br>
<h3>
Responsabilidad social empresarial integral<br>
“Compromiso con la sociedad”
</h3>

- Qué es?

Es personal, es gerencial, es empresarial, es social, es de
todos!

- Qué abarca?

Es un compromiso, es ético, es voluntario, es cultura, son
valores, es cooperación.
Se sustenta en los objetivos del Milenio

- Cuánto tiempo?

No es a corto plazo, es a largo plazo-duradero. Busca el
desarrollo sostenible en salud, educación, costumbres, social
y económico.

- Cómo?

Donaciones en tiempo , dinero, compra de portafolio,
acompañamiento a los proyectos.

</div>
<div class="ta-c" style="width: 256px">
    <img width="256px" src="/static/apoyenos.jpg"/>
    <a href="paypal.me/ApoyenosCHC">PayPal</a>
    <div>
        Para donaciones en dinero están habilitadas las siguientes cuentas bancarias, estamos autorizados de expedir certificados de donación:

<b>En Colombia:</b>

Cuenta corriente Banco Colpatria
Corporación Horizontes Colombianos
N° 4431003061
Bogotá

<b>En Alemania:</b>

Berthold Wolff
TARGOBANK
N° 0203653632
BLZ 30020900
    </div>
</div>
</main>
