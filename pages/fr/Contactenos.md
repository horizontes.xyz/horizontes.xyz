---
date: 2019-01-06
identifier: Contactenos
title: Contact
layout: layouts/contacts.njk
permalink: /{{ title | slug}}.html
tags:
 - nav_fr
---

<center>

Corporación Horizontes Colombianos – CHC
CR 6B Este No 88G – 19 Sur/ Bogotá D.C. – Colombia
Tels.: (310) 254-5321/ (312) 604-9037/ (320) 495-9889
<a href="mailto:corporacionhorizontescol@gmail.com">corporacionhorizontescol@gmail.com</a>
https://horizontes.xyz
</center>
