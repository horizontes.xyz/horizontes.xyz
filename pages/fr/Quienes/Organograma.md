---
date: 2019-01-04
identifier: organograma
permalink: /{{ locale }}/{{ parent | slug }}/organigramme.html
title: Organigramme
layout: layouts/chart.njk
---
<pre class="mermaid">
  graph TD

  A[Assemblée Générale] --- B1[Contrôleur Financier]
  A --- B2[Conseil des <br/> Associés]
  B2 --- C1[Représentant <br/> Légal]
  C1 --- D1[Représentant <br/> Légal]
  C1 --- D2[Représentation <br/> Indigène]
  D2 --- E1[Représentant <br/> en Allemagne]
  E1 --- F1[Directeur <br/> Adjoint]
  B2 --- C2[Communicateur <br/> Interculturel]
  C2 --- D3[Responsable <br/> Culturel]
  D3 --- E2[Conseiller de Projet]
  E2 --- F2[Coordinateur <br/> Environnemental]
  F2 --- G2[Conseiller <br/> Juridique]

  classDef someclass fill:#f96;
  class A someclass;
</pre>
