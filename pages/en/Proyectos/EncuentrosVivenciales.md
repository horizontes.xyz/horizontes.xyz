---
identifier: EncuentrosVivenciales
title: Living Encounters
img: encuentrosvivenciales_foto_1.png
layout: layouts/gallery.njk
pics: 8
---

The project was created after profound reflections on sharing, interconnecting, reconciling and self-awareness.

Far from nature, in noisy cities, polluted air and stressful routines, we are condemned to lose the sense of the essentials of life and become prisoners of our own inventions.

The experiential encounters allow the participants to share knowledge and practices in natural territories, together with communities that work in the preservation of these magical places, enjoying and taking advantage of the silence for a wide, deep and intimate introspection about the world and life.

This is of particular interest for public and private companies, educational institutions and the general public, with the need to relax and expand the notion of time.
