---
identifier: Educacion
permalink: /{{ locale }}/{{ parent | slug }}/education.html
title: Education - basis for a dignified life
img: proy_edu_foto_1.jpg
notopimg: true
layout: layouts/page.njk
---

## Background

Through the training of professionals in the different areas of knowledge, CHC tends to this day to empower the population, providing indigenous youth with access to university studies in different careers that they choose themselves.

![](/static/proy_edu_foto_1.jpg)

Within the program, 15 indigenous people from different ethnicities of Colombia, women and men, have completed their superior career, with full and partial support, and fulfilled their commitment to the CHC, distributing their requested knowledge to their origin communities and transmitting their experiences during their permanency at university, therefore in cities like Bogotá, Palmira and Medellín.

Currently we are working to carry out this initiative to other communities in the country.   

{% set path = identifier + '/Antecedentes' %}
{% set pics = 8 %}
{% include "components/gallery.njk" %}

## Present

Education, as a fundamental right, ensures in particular for ethnic minorities such as indigenous and afro-descendant communities, an active participation in the social and cultural development of the country. Marginalized for centuries they have suffered violence and disregard of their culture for society in general, due to immense indifference. Accessing university studies, the people have the opportunity to make them known and to be at the same level of opportunity as their white co-students, exercising their intellectual capacity and sharing their ancestral knowledge.

![](/static/proy_edu_foto_2.jpg)

Currently, CHC is supporting 5 students (women and men of the Nasa people from Cauca) in their university training in different careers at the Universidad Surcolombiana in the city of Neiva - Huila. Young responsible leaders have the task, within the communities they belong to, to provide advice through their acquired knowledge.

{% set path = nil %}
{% set pics = 14 %}
{% include "components/gallery.njk" %}
