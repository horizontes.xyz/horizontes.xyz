---
identifier: Investigadores
title: Researchers in school stage
img: investigadores_foto_1.jpg
---
The project is designed from the initiative to support tenth and eleventh grade students of the José Reyes Pete Educational Institution, headquarters Ethno-Ecological Technical Institute "Cxamb Wala" (Pueblo Grande), located in the Indigenous shelter of Vitoncó (Páez-Cauca), who develop ethno-ecological proposals as a degree requirement. There is a great interest in contributing to cultural survival from the feeling of indigenous youth; from these initiatives, their own traditions and knowledge are rescued, they are investigated according to worldview and beliefs, accompanied by the elders and community leaders, strengthening intergenerational communication. By this means it seeks to open the way to position spaces for youth participation, built from cultural autonomy, where there is a deep respect for Mother Earth, the territory and the spirits that inhabit it. All actions developed to achieve the goals prioritize the WËT FIZENXI, meaning: to live well and in harmony with each other.

<video width="90%" controls>
 <source src="/static/Presentación_Deny.mp4" type="video/mp4">
 Your browser does not support the video tag.
</video>

The -CHC- was pleased to support the process of creating the traditional story collection booklet: “Yeckatê´we´sx (yuwetx katxude ec)”, an initiative carried out by the Nasa indigenous student Denny Milena Cayuy like her bachelor's degree work at the José Respetes School.
The support of this initiative is a joint achievement that aims to strengthen their own traditions and knowledge, promoting the preservation of the oral and written language, making these stories reach mainly the children and youth of the indigenous community. This is how these stories are published with editorial quality and images made by the children of the community, making this a process of the community that thinks of itself, from their interests and needs, such as the preservation of their own culture, in the context of globalization that tends to commercialize and homogenize cultures. For this reason it is very important to strengthen one's own identity through this type of initiatives that aim to make children and young people knowledgeable and proud multipliers of their indigenous history.
We are currently working on obtaining resources with the aim of making a printed version in hard paste and with analysis in Spanish of the stories, to spread the work in other Nasa indigenous reservations and educational institutions in the departments of Cauca and Huila.
