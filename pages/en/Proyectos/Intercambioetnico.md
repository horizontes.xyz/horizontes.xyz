---
identifier: Intercambioetnico
permalink: /{{ locale }}/{{ parent | slug }}/ethnic-cultural-exchange.html
title: From the distant dawn to the awakening of the new continent
img: intercambioetnico_foto_1.jpg
layout: layouts/gallery.njk
pics: 12
---

Research Proposal - Documentary: convergence points between different ethnic groups of Colombia and China.

The ethnic-cultural exchange project is focused on the investigation, documentation and approach of the different points of convergence, between the ethnic groups of Colombia and China, with which the CHC performs joint work. This initiative seeks to strengthen the ties of brotherhood and exchange between the ancestral peoples of the two continents, collectively building efforts that through culture allow us to understand ethnographic similarities and differences, but also the forms of collective thinking that permit us to help in the work of living in harmony with nature and other beings, inhabitants of the earth.
