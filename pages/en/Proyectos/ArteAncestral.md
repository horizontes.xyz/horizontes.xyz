---
identifier: ArteAncestral
layout: layouts/gallery.njk
title: Ancestral Art
img: ArteAncestral/10.jpg
notopimg: true
pics: 10
---

<div class="fr pa-l">
<img width="256px" src="/static/tienda_en_linea.jpg"/>
<h3>Online Shop</h3>
</div>

Project for the promotion of handicrafts of different indigenous people.

The project was born from the proposal made by the Tikuna communities and the CHC "Cooperativa de Artesanos del Trapecio Amazónico" (Cooperative of Artisans of the Amazon Trapeze) which sought to integrate the Indigenous Communities in productive spaces, cultivating the skills of association and cooperativism, seeking the rescue, conservation and promotion of the ancestral work of the communities.

With the intention of resuming and extending this project to other communities with which the CHC work in cooperation, we intend to open distribution and sales channels, through alliances and under the concept of Fair Trade, thus improving the income and living conditions of artisans in the different communities linked to the project.

This project is carried out with the support of the Association of the corresponding Indigenous Councils and entities such as ACITAM, the Francisco José de Caldas District University and the Student Council of the Universidad Surcolombiana.

Currently, the CHC seeks to strengthen and link different traditional artisans to an interactive platform where they can exhibit their work, seeking allies within the system of fair trade.
