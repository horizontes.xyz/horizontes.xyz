---
date: 2019-01-01
identifier: ParteDelArte
title: Parte del Arte – Gallery
img: parte_del_arte_foto_2.jpg
notopimg: true
layout: layouts/gallery.njk
pics: 9
---

<img src="/static/parte_del_arte.jpg"/>
The Corporatión Horizontes Colombianos, CHC, began in October 2003 its cultural work in the “Part of Art - Gallery” space, located at that time in the CHC headquarters.

This project was created with the intention of establishing an exchange between Colombian and German artists. It had a physical space for 4 years, where multiple collective and individual exhibitions of Colombian and German artists were held, such as: Carlos Rojas, Edgar Negret, Darío Ortiz, Doris Hinzen - Roehrig, Umberto Giangrandi, Mario Salcedo, Jim Amaral, Gladys Ortiz among many other artists.

This space was open to exhibitions by artists with disabilities such as Aydée Ramírez, to excellent little-known artists and also to exhibitions of students of the Pontificia Universidad Javeriana.

The gallery was known for its constant exchange and for its artistic and social activities.

Exhibitions held by the CHC:

"The children  count as well." Collective exhibition, organized by the CHC and UNICEF Colombia.

"Identity - Reality." Collective exhibition. A tribute to the artist Carlos Rojas, with the participation of Gladys Ortiz, Mario Salcedo and works by maestro Rojas.

"Minotaur Concert". Individual exhibition of Nubia Medina.

"Arrival in restless sea". Exhibition of the German artist Uwe Anders.

"Visual contrasts." Individual exhibition of the artist Mari Rincón / Medellín.

"The silence of the light." Individual exhibition of the artist Germán Méndez.

"Usurped." Individual exhibition of the painter's social character by mouth-artist Aydée Ramírez

"Sky Line." Exhibition of the German artist Doris Hinzen Roehrig from Berlin.

"Women in Art." Collective exhibition of 4 women linked to the CHC in the ECOPETROL Club.

"Concrete vision." Individual exhibition of the artist Raquel Ramírez from Bucaramanga.

"A view from the heart." Individual exhibition of the artist Jhon Jairo Restrepo from Quindío for a partly blind and partly sighted audience.

"What we carry inside." Exhibition of the German artist Petra Jung, accompanied by an audio - visual installation by the Colombian artist Antonia García.

“Interior view - Exterior view”. Collective exhibition in Berlin - Germany with the participation of Colombian artist Mari Rincón from Medellín, linked to the CHC.

"Angels in Art." Collective exhibition with the participation of Colombian and German artists.

Exhibition of three thesis works of students of the Fine Arts Career / Pontificia Universidad Javeriana.

"A view from the heart." Traveling exhibition by artist Jhon Jairo Restrepo, organized by the CHC in Cali / Valle.

"Grabve - Terrible." Collective exhibition of three Colombian artists, showing their engraving works.

"Memories of the city." Exhibition of three student artists; thesis works of the Fine Arts Career / Pontificia Universidad Javeriana.

"Panorama Vision." Collective exhibition of Colombian, German and North American artists.

"Bathroom experiences ..." Individual exhibition of an artist with her degree work of the Fine Arts Degree / Pontificia Universidad Javeriana.

"Ancestral heritage" - Large exhibition of indigenous art, accompanied by photographs, videos and sound of the Amazon, with the participation of students from different universities in Bogotá.

"Looking for footprints." Exhibition of the Colombian sculptor Oscar Pulido; He lives and works in Cologne / Germany and exhibited for the first time in Colombia.

"Continuity of nature through urban development". Collective exhibition by artist Kevin Nazar from Honduras and Maria Andrea Umaña from Colombia.

"Natural innuendo." Individual exhibition of the artist Angélica Chaparro; graduated from the National University of Colombia.

"Cosmic Explosion." Individual exhibition by artist César López of his new watercolor work, accompanied by a video clip of the CHC "Five variations and a requiem".

Closing of the “Parte del Arte - Gallery” space in 2007 due to lack of financial support.

The CHC has done a great job with this project, including all strata, such as artists and spectators, awakening the taste for one of the fine arts, which are the plastic arts, which goes against violence, which is very present in the country.

We have been able to raise funds for the CHC, also supporting artists, who are increasingly experiencing a complicated situation, always pleasing the people, who have visited this space.

Through “Parte del Arte - Gallery” many people learned about the social management of the Corporación Horizontes Colombianos.

It is intended to resume the project through the Internet.

<video controls>
 <source src="/static/parte_del_arte.mp4" type="video/mp4">
</video>
