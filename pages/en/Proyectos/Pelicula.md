---
identifier: pelicula
permalink: /{{ parent | slug }}/the-origen-of-the-tikuna-people.html
navtitle: 'Movie: “The Origin of the Tikuna People””'
img: poster_film.jpg
---

# “The Origin of the Tikuna People” - The Rescue of a Myth and a Language

Seeking to make visible the Indigenous communities of Colombia, the CHC made, together with the community, their first ethnographic feature film “The Origin of the Tikuna People”, which was recorded in 2008, awarded as Best Debut in the Big Apple Awards of Latin American Cinema 2009 - New York, and ICT Award Ministry of Culture of Colombia.
This is an initiative of the Indigenous community of Nazareth, which arises as a project of the IBURI Channel (community television channel).

The film narrates according to ancestral beliefs, the origin of this ethnic group that extends through Brazil, Colombia and Peru, thus achieving an audiovisual memory of the tradition revealed by oral means for centuries.

This first plot movie, filmed with the participation of the Nazareth - Amazonas Indigenous, is the first production in Colombia, recorded in a language other than Spanish, with indigenous actors and more than 60% of the technical staff, also Indigenous, it had an Award from the Ministry of Culture in the ICT Modality with social inclusion, the support of the University Politécnico Grancolombiano and the Corporación Horizontes Colombianos. It was subtitled in Spanish, German, English, French, Portuguese and Italian. It is worth noting that the film has become an element to make visible ethnic groups of the Amazon.


 - [TRAILER](http://www.youtube.com/watch?v=zYQb4f-CaGU)
 - [Movie](https://www.youtube.com/watch?v=cN-SG6nASLQ&pbjreload=10)

Behind the scenes
 - [Research and development of the movie](https://www.youtube.com/watch?v=y85Z83XJYLE)
 - [Preproduction](https://www.youtube.com/watch?v=gbkFmHw5WXo)
 - [Production](https://www.youtube.com/watch?v=z64X-gx5oj4)
 - [Postproduction](http://www.youtube.com/watch?v=KiueXm8wX84)


<div class="fr pa-l">
<img src="/static/Pelicula/1.jpg" />
</div>


**From the Amazon to the Big Apple**
From a minority to the world
“The Origin of the Tikuna People”


<div class="fl xpa-r mw-5">
<img src="/static/Pelicula/2.jpg"/>
</div>

The jury of the “The Big Apple Latin American Cinema Awards” gave, on September 17, 2009, the trophy in the category of Best Debut to the feature film “The Origin of the Tikuna People” that tells the creation myth according to the Indigenous of the Amazonian Trapeze.

<div class="fr pa-l mw-5">
<img src="/static/Pelicula/3.jpg"/>
</div>

This film, which unveiled the situation of an ethnic minority of the Amazon countries and whose actors were natives of these ethnic groups, was labeled as sacred by the elders of the tribe and can now be seen in many scenarios that value these cultural and artistic manifestations. For the CHC it is a pride to share with the general public, with those who support their work and with those who barely hear them name, the joy that beats in the hearts of their protagonists, members of the Indigenous community, of their director (Gustavo de la Hoz) and its producer (Dorothea Wolf Nurnberg) who is also the founder and legal representative of the Corporación Horizontes Colombianos.
Congratulations to the Indigenous actors who interpreted their history for those who did not know her.

**The CHC made presentations in the following university and cultural spaces:**
**Launch of the feature film “The Origin of the Tikuna People” in the Tikuna Nazareth community - Amazonas**

<div class="fl pa-r mw-10">
<img src="/static/Pelicula/4.jpg"/>
</div>

At the beginning of February 2009 the film was presented exclusively in the Nazareth-Amazonas tribe, this was an act full of magic, joy and optimism, which was attended by the protagonists, the technicians, the community in general and four professors of the University Javeriana Bogotá, invited by the Corporación Horizontes Colombianos. This is the first time that the Tikuna people have seen one of their sacred myths on the screen.


**Successful Première of the Feature Film “The Origin of the Tikuna People” at the University Politécnico Grancolombiano in Bogotá, with the presence of characters from the Culture and some protagonists of the cinematographic work.**

<div class="fr pa-l mw-15">
  <img src="/static/Pelicula/6.jpg"/>
</div>

The film “El Origen del Pueblo Tikuna”, which has an award from the Ministry of Culture, was presented on April 28, 2009 at the auditorium of the University Politécnico Grancolombiano with the presence of the Bogotá technical team, its director Gustavo de la Hoz, the producer Dorothea Wolf-Nurnberg and its protagonists, Juvencio Pereira Manduca and Yenika Mojica Pereira, of the Tikuna ethnic group.
<div class="fl pa-r mw-10 ">
<img src="/static/Pelicula/7.jpg"/>
</div>

Dr. Pablo Michelsen Niño, Director of the Politécnico, referred to the production as a sample of the work of social projection of the important educational institution and as a reflection of what they have been doing in cinematography with applications of academic activity in the national environment.

<div class="fr pa-l mw-10">
  <img src="/static/Pelicula/8.jpg" />
</div>

For his part, Carlos Augusto García, Director of the Faculty of Communication and Arts Sciences expressed his satisfaction with this work that, in his words, renews and reinvents pedagogy with an exercise in the creation and presence of collective memory with the myth of the creation of the Tikuna people of the Amazon.

<div class="fl pa-r mw-8">
<img src="/static/Pelicula/9.jpg" />
</div>

The Ministry of Culture, through Germán Franco, highlighted how this film shows the cultural diversity of the country and recognized the inclusion in this great cinematographic effort of those who have long been excluded.
Silsa Arias also had an outstanding participation on behalf of the National Indigenous Organization of Colombia - ONIC, who expressed the importance of opening the academy to those who have been invisible in our society to allow them to make their contribution to the Colombian society from the view of their people. She also left us as a teaching that this myth is a reality for Indigenous peoples.
<div class="fr pa-l w-10">
  <img src="/static/Pelicula/10.jpg" />
</div>

The thanks to the Tikuna community for opening their doors to the work team, led by the Corporación Horizontes Colombianos, interacted with their members to reach this feature film that gave much to talk about in the different scenarios where it was exhibited.
On May 5, 2009 the film was presented, in two functions, to the university community in general with their respective film forums.

**The Zuá Foundation enjoys “The Origin of the Tikuna People”**
<div class="fl pa-r w-15">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/11.jpg'">
    <img src="/static/Pelicula/11.jpg" />
  </a>
</div>
On May 23, 2009 the film was screened at the Zuá Foundation in Sassaima - Cundinamarca, with the participation of the CHC Indigenous students, who in part starred in the film, sharing this experience with the beneficiaries of the Zuá Foundation, who are marginalized people, residents of Patio Bonito and who through the Foundation have the possibility of studying a technical or professional career. This exchange left a teaching for both parties. The leaders of the two non-profit organizations also attended this meeting.

**“The Origin” at the National University of Colombia - Medellín Headquarters**
<div class="fr pa-l w-10">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/12.jpg'">
    <img src="/static/Pelicula/12.jpg" />
  </a>
</div>

On August 1, 2009, the film was presented to a small group of students, especially Indigenous people from different ethnicities and regions of the country. Once the projection was finished, a very interesting dialogue was developed about the realization and its purpose.

**“The Origin of the Tikuna People” at the opening of the Charles Darwin Symposium, The Tadeo Lozano University in Bogotá pays tribute to the man of science**

<div class="fl pa-r w-10">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/13.jpg'">
    <img src="/static/Pelicula/13.jpg" />
  </a>
</div>

On August 18, 2009, in the auditorium Maximum  of the Tadeo Lozano University, the film “The Origin” was presented as part of the Symposium organized by the Humanities area, Marine Biology and the Center for Art and Culture.
A forum was held in which Mauricio Laurence was moderator and included among its participants Gustavo de la Hoz of the CHC, Director of the film and Yenica Mojica Pereira, protagonist Tikuna. It should be noted the massive participation of students and their interest in knowing the subject more thoroughly.

**The "Origin of the Tikuna People" in Alternate Space of Uniandinos -
Graduates of the University Los Andes in Bogotá**

<div class="fr pa-l w-10">
  <img src="/static/Pelicula/14.jpg" />
</div>

On September 8, 2009, the film of the Corporación Horizontes Colombianos “The Origin of the Tikuna People” was presented in Alternate Space to pay tribute to the Tikuna culture, organized by the Social Alliance and the Cultural Subdirectorate of Uniandinos.
 For this presentation were invited Miguel Dionisio Ramos, Juvencio Pereira Manduca, Yenica Mojica Pereira and Rosenaida Dionisio Quirino, protagonists of the film, as well as Executive Producer Dorothea Wolf - Nurnberg and Director Gustavo De La Hoz, who participated after the screening at the cinema forum. As an accompaniment an artisan exhibition was organized in the same space.


**“The Origin of the Tikuna People” for Film and Television students**   

<div class="fl pa-r w-15">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/15.jpg'">
    <img src="/static/Pelicula/15.jpg" />
  </a>
</div>

On October 6, 2009, the Cinema and Television students enjoyed the screening of the film at the Lumière Foundation facilities. This film production had a great impact on future filmmakers, understanding the new proposal in the Colombian Cinema scene.
The cinema forum was offered, as a custom and a necessity, to clear up many concerns with the indigenous assistants and the Director and the Executive Producer of it.

**Confrontation of the movie "The Origin of the Tikuna People" with Anthropologists, Sociologists and Historians**

<div class="fr pa-l w-10">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/16.jpg'">
    <img src="/static/Pelicula/16.jpg" />
  </a>
</div>

On October 8, 2009, the feature film for students of the Anthropology, Sociology and History Careers was presented at the Pontificia Universidad Javeriana.
Due to the limited time, a long-awaited cinema forum by the CHC and the indigenous participants could not be held, but instead a small conversation.

**“The Origin of the Tikuna People” and the Environment**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/17.jpg" />
</div>
On October 16, 2009, the film for the students of the careers, related to the Environment, was presented at the Francisco José de Caldas District University in Bogotá.
“After the 517 years of discovery of America, our present, our violence, our forgetfulness, our behaviors, are the consequence of acculturation in America. The suffering of our peoples and their current agony, remember that the historical persecution of our roots is not over yet.
Rescuing our indigenous roots is of vital commitment to our living past and our uncertain future ...”Rafael - Student of the Environmental Administration Degree.

**Second presentation at the Pontificia University Javeriana**
<div class="fr pa-l w-15">
  <a onclick="modal.style.display = 'flex'; changeMe.src = '/static/Pelicula/18.jpg'">
    <img src="/static/Pelicula/18.jpg" />
  </a>
</div>

This presentation of the "Origin of the Tikuna People", on October 19, 2009, was open to the public and was held at the Troncoso Marine Auditorium of the Pontificia University Javeriana.
Students from different universities of Bogotá were found to contemplate the film and share opinions in the forum. This projection was accompanied by the Indigenous and representatives of the CHC. Also a sample of the ancestral art Tikuna was realized.

**“The Origin of the Tikuna People” at the V International Film Festival of Pasto**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/19.jpg" />
</div>
In August 2009 the meeting of the Shamans, Mamos and Taitas (1st International Meeting of Andean and Pacific Cultures) was held in Pasto as an exchange of knowledge and development of conferences on traditional medicine without neglecting the ceremonies, the rituals, songs and dances, among other artistic manifestations.
The countries participating in this opportunity in which Colombia was the host were: Chile, Venezuela, Argentina, Bolivia, Peru and Ecuador.
At the same time, the 5th International Film Festival of Pasto was held, with the participation of professionals from Film and Television, as well as the assistance of national and international speakers.

<div class="fr pa-l w-15">
 <img src="/static/Pelicula/20.jpg" />
</div>

The Corporación Horizontes Colombianos represented by Dorothea Wolf - Nurnberg who serves as Legal Representative of the CHC was a special guest with "The Origin of the Tikuna People", as a film out of competition.
 Dorothea, who is also the Executive Producer of the Film, was invited to the Panel that was organized at the University of Nariño with the documentary filmmakers Martha Rodríguez, from Colombia, Juan Martín Cueva Director of the Documentary Film Festival “Zero Latitude” of Ecuador, Humberto Mancilla, Director of the Human Rights Film Festival of Bolivia and the Governor of the Department of Nariño, Antonio Navarro Wolff. Emphasis was placed on memory and audiovisual productions.

**Invitation to the 2nd International Festival of Alternative and Community Film and Video "Ojo al Sancocho" in Bogotá D.C.**
<div class="fl pa-r w-10">
  <img src="/static/Pelicula/21.jpg" />
</div>

On September 22, 2009, the Corporación Horizontes Colombianos was invited with the film “El Origen del Pueblo Tikuna”, to the second edition of the International Festival of Alternative and Community Film and Video “Ojo al Sancocho” in Ciudad Bolívar in Bogotá. On this afternoon, the trailer of the film was presented, accompanied by a conversation, with the presence of delegates from Indigenous and Afro-descendant peoples, representing Bolivia and Colombia. Likewise, the Legal Representative of the CHC and Executive Producer of the film, as well the Director Gustavo De La Hoz participated in this dialogue table.

**1st Indigenous Film and Video Exhibition in Colombia “Daupará” - To see beyond**
<div class="fr pa-l w-15">
  <img src="/static/Pelicula/22.jpg" />
</div>

This project is part of a common proposal of the Indigenous communities and Peoples throughout the Americas. Today more than ever, when our Mother Earth slowly agonizes, voices, images, feelings, bets, and struggles of Indigenous Peoples join in their defense. From their own optics and hands, they will be restored to the Colombian collective imaginary through the First Indigenous Film and Video Exhibition in Colombia, whose base was a multicolored backpack filled with audiovisual fruits made and rethought by members of indigenous peoples and communities of our country in recent years, under the impulse and attunement of winning productions at the IX International Festival of Film and Video of Indigenous Peoples, developed in Bolivia in 2008.
The Corporación Horizontes Colombianos was invited with its film “The Origin of the Tikuna People” and to participate in four working groups, where the following topics were discussed: Experiences in Indigenous Communication, Knowledge Exchange between Indigenous and Non - Indigenous filmmakers, Public Policies and Ethnic Communication, Media and Ethnic Representation. The Origin was presented at the National University of Colombia within the framework of the Festival.

**1st San Agustín Film and Video Festival**
<div class="fl pa-r w-10">
<img src="/static/Pelicula/23.jpg" />
</div>

This nascent audiovisual media event took place from November 13 to 16, 2009. It was four days to appreciate the development of the new Colombian cinema, emphasizing ethnographic and social cinema and video.
The audience had the opportunity to talk with directors, screenwriters and academics during the seminars, workshops and conversations that took place within the framework of the Festival.
“The Origin of the Tikuna People” was part of the program, together with audiovisual productions from Mexico and Colombia.

The movie was also presented in cultural and university spaces, as well as in Cinematographic Festivals in Bolivia, Germany, Italy, France and Spain.
