---
date: 2019-01-06
identifier: Apoyenos
title: Support us
layout: layouts/base.njk
permalink: /{{ title | slug}}.html
tags:
 - nav_en
---

# THE VALUE OF SOLIDARITY

<main class="flex jc-c">
<div class="pa-l-2 pa-r">

<iframe width="888" height="666" src="https://www.youtube.com/embed/LFeuJWNyCHU" title="CHC- Apoyenos - Suport us" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Choose the way to support:</h2>
<h3>Sponsorship - Pa Chawene “A hand for my brother”</h3>
<a href="/static/Plan-Padrino_pa-chawene_en.pdf"> Presentation </a>
<br>
<h3>
Comprehensive corporate social responsibility
"Willingness to commit to society"
</h3>

- What is it?

It’s personal, it’s executive, it’s businesse, it’s social, it’s for
everyone!

- What does it cover?

It's willingness to compromise, it's ethical, it's voluntary, it's
culture, it's values, it's collaboration. It is based on the
Millennium Development Goals.

- How long?

It's not short term, it's long term. It strives for one sustainable
development in health, education and habits, social and
economic in nature.

- How?

Donations in time, money, project support.

</div>
<div class="ta-c" style="width: 256px">
    <img width="256px" src="/static/apoyenos.jpg"/>
    <a href="paypal.me/ApoyenosCHC">PayPal</a>
    <div>
        For cash donations the following bank accounts are enabled, we are authorized to issue a donation certificates:

<b>In Colombia:</b>

Cuenta corriente Banco Colpatria
Corporación Horizontes Colombianos
N° 4431003061
Bogotá

<b>In Germany:</b>

Berthold Wolff
TARGOBANK
N° 0203653632
BLZ 30020900
    </div>
</div>
</main>
