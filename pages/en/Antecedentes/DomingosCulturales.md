---
identifier: DomingosCulturales
layout: layouts/gallery.njk
title: Cultural Sundays
img: DomingosCulturales.jpg
pics: 3
---
The project can be referred to as one of the first that CHC launched in order to bring art to the popular classes, giving the opportunity to contemplate, analyze, discuss and practice it in a didactic framework in direct communication with some artists and their work.

We have sought through this contact with art a transformation in people, to achieve a balance within each one, a better understanding of the world and life, leading them to a positive attitude.

We focused our work in “Consuelo Sur”, a popular neighborhood at the southern part of Bogotá, in conjunction with the Community Action Board to determine the sites and ways in which the community was convened around artistic practices (music, theater, contemporary dance, literature, poetry, documentaries on different topics).

CHC received donations in form of refreshments, in particular for girls and boys, and small useful gifts.

We included in our visits another project that we designed called “Garbage does not equal garbage”, in which we encouraged people to reuse and recycle, generating experiences of artistic creation with the supposed garbage, from there came pieces of art, under the direction of plastic artists.

On the back of a truck and with megaphone we toured the streets to summon the population; on the soccer field of the neighborhood we found the most suitable place to work for long hours and with excellent participation of the inhabitants, which allowed sharing with the community, generating spaces of peaceful and artistic coexistence.
