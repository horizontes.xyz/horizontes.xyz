---
identifier: Artesanos
layout: layouts/gallery.njk
title: Building a cooperative for artisans of the Amazon - Trapeze
img: artesanos_foto_1.jpg
pics: 12
---

With this project, we intended to achieve a comprehensive social development of the native people of the Amazon Trapeze, to create productive spaces and to promote the skills of the cooperation and cooperative among artisans, in order to revive the work of their ancestors and to stimulate the region in general.

At the same time, channels for distributing and selling products through alliances and own points of sale within the framework of the concept of fair trade have been established to improve the income and living conditions of 1,500 artisans in the 18 tribes and to link them to this project.

This initiative was planned with the support of the Association of Indigenous Cabildos of the Amazon Trapeze - ACITAM, the headquarters of SENA Leticia, the advice of the Julio Mario Santo Domingo Foundation, and the University Francisco José de Caldas on environmental issues.
