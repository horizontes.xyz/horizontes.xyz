---
identifier: Iburi
layout: layouts/gallery.njk
title: IBURI - Our Indigenous View
img: IBURI_foto_1.jpg
pics: 14
---

This was a closed-circuit Television Channel with a programme specially focused on the Nazareth Amazonas community and the surrounding Tikuna communities. The objective was to strengthen the Tikuna culture through the presentation using audiovisuals, showing important issues about their culture, while rescuing and preserving ancestral practices as audiovisual memory for future generations.

The monthly program was broadcast in Tikuna and Spanish, where a news program with high regional content, documentaries, short films, video clips, movies, campaigns for education and for the recovery of values ​​were programmed. All content was prepared in conjunction with the community. Audiovisual equipment was supplied and the director Luz Dary Mojica was appointed for its future operation, where the indigenous people will make their own programs, including their broadcast within the community and the other Tikunas communities of the Amazon.

Within the framework of the project, the movie “The Origin of the Tikuna People” was made.

IBURI was an accomplishment of the CHC with the support of the Grancolombiano Polytechnic.
