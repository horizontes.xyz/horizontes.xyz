---
identifier: Sanitation
parent: Background
layout: layouts/gallery.njk
title: Basic sanitation
img: Sanitation/12.jpg
pics: 14
permalink:  /{{ locale }}/{{ parent | slug }}/{{ title | slug }}.html
---

The “pilot project for the implementation of basic sanitation methodologies and strategies in indigenous communities of Colombia, case: Tikuna Nazareth Community - Amazonas”, was an initiative of the CHC in partnership with the Department of Civil Engineering of the Pontificia University Javeriana / Bogotá, which sought to improve the sanitary conditions of the community by improving the supply of drinking water, sewerage and a proper management of solid waste.

The studies and designs were documented and endorsed by the Association of Indigenous Cabildos of the Amazonian Trapeze (ACITAM), the Ministry of the Environment, Corpoamazonía, the Government of the Amazon and the Mayor's Office of Leticia. The tribe took responsibility for raising funds in Spain to realize this project.
