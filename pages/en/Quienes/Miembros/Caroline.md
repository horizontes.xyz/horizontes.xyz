---
identifier: caroline
title: Caroline Schiller
img: Miembros/Caroline.jpg
---

Volunteer teacher in a Waldorf school in Bogotá (Inti Huasi, "Casa del Sol"), teaching English, German and helping in the support group, - with disabled children.

Double Degree in Business Management and Economics (University of Glasgow, Scotland)
Born 20th May 1995 in Munich, Germany, though both parents are from Czech Republic. After four years of Business and Economics studies, and several jobs in that direction (including the position in Deloitte’s consultancy company in Leeds and the sales job in Zionsville, Indiana with Southwestern Advantage) she decided to step away from that world, and dedicate her coming years to projects that were not focused on finance, sales, commerce and business.

Volunteering at the Eco-farm Can Decreix (Cerbere, France) gave her the opportunity to discover economic alternatives, i.e. “Degrowth”,  and movements such as Slow Food, and Foodsharing, fighting against the food waste problem and the loss of the use of local foods. Some of the other places of volunteering in Europe include the Eco Krishna Farm in Lesmahagow (Scotland), Mirapuri (in Mirapuri-Coiromonte, Italy) and  Menschliche Welt (Yoga Ashram in Germany near Ravensburg), each of which helped her to find more understanding in living in harmony, community, rather than competition, - including our co-existence with nature.

Currently, the Waldorf School "Inti Huasi" (Casa del Sol) has extended and brought her focus on education, which is also her aim and passion as a member in Corporacion Horizontales Colombianos (CHC) in Colombia.
