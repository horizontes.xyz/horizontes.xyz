---
identifier: berthold
title: Berthold Wolff
img: Miembros/Berthold.jpg
---

Born on March 2nd, 1965 in Hilden. 1984 Abitur at the Helmholtz-Gymnasium Hilden. 1989 studies of theology in Bonn and Vienna with diploma. 1992 ordination in Cologne; thereafter two chaplains of 4 years each in Brühl and Bensberg.

Since 2000 pastor at the Archbishop's St. Ursula Gymnasium and at the Elisabeth von Thüringen Realschule in Brühl. In September 2009 he was appointed parish priest in the parish of St. Maximilian Kolbe in Cologne Porz and 10 years later he was pastor of the ‘Porz broadcasting room’. Since 1993 founding member and chairman of the non-profit association for the promotion of training of young people in the Dominican Republic (AJD e.V.) As part of this activity, he has been traveling to the Dominican Republic every three years with a group of interested people since 2000.
