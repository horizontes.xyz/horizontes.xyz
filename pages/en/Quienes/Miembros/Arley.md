---
identifier: arley
title: Arley Buitrago Landázuri
img: Miembros/Arley.jpg
---

Afrodescendant, born 1985 in Bogotá. He has a degree in Social Sciences and a Master's Degree in Art Studies with experience and vocation for community work. He has worked with the School of Social Art “Videos y Rollos”, the “Entre-Tránsitos” Collective, the “Cimarron” National Movement and the “Corporación Horizontes Colombianos” (today associate member) during the last 5 years. In the constant interest to articulate cultural and community artistic processes, he has developed artistic practices such as performance and video art with a gender, intercultural and interdisciplinary approach. He has published the articles "Memories of a continued agony", "The magical religious practice of Candomblé in Bogotá" and "The schism of inhabiting a black body".
