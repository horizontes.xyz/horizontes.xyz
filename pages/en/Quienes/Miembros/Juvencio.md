---
identifier: juvencio
title: Juvencio Manduca
img: Miembros/Juvencio.jpg
---

Member of the Tikuna Ethnic Group.

He was born in the Nazareth Community in the Colombian Amazon on March16th. He went to primary school in the “Concentración Escolar Nazareth (CEN)” and he took the 5th grade in the Indigenous Institute San Juan Bosco.
He finished High School and afterwards he took the training course of Film & Television Production and Direction“Imaginando nuestra imagen” (Imagining our image) – INI, offered by the Ministry of Culture in the National University of Colombia – Chapter Amazon (2003). He entered the program “Education – basis for a dignified life” of the Corporación Horizontes Colombianos in 2006 and took a semester of Fundamentals in the National University of Colombia, chapter Amazon and the took the Fundamentals Semester (2007) and the pre-college course in Bogota (2007).

Due to economic difficulties, he could only take the summer middle school, however for 8 years after finishing high school he has shared his knowledge with the community, leading initiatives with the traditional authorities.
He is currently fulfilling his dream of going to Law School after joining -CHC- in January 2006 as an honorary member. Since January 2008 he is an associate and the Secretary of the CHC General Assembly.

His great effort is supported by his parents, Pedro Pereira Ramos and Ligia Manduca Guillermo, who are agricultures in the Amazon. He is also married to Luz Emerita Moran Ahue and has a son, José Camilo Pereira Moran.
