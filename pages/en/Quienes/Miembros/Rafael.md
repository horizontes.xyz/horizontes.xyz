---
identifier: rafael
title: Rafael Antonio Chaparro Torres
img: Miembros/Rafa.jpg
---

33 year old.
Region: Tota, Boyacá, Colombia
Environmental Administrator with a Master's degree in Geographic Information Systems.
Descendant of indigenous-peasant grandparents interested in traditional communities and the millennial knowledge of Latin American ancestral peoples. Since 2009 he has participated in different initiatives and socio-community programs related to ancestral thinking, environmental education and the recognition of territories (Mayan route, Inca route). Until 2014 director of the “Pachakutia” Collective, a youth organization aimed at environmental education based on ancestral values and community art.
Volunteer member of the Corporación Horizontes Colombianos - CHC for 14 years, collaborator in management and formulation of projects, as well as in research and environmental affairs. Since June 2019 associate member and Assistent Manager of the CHC.
