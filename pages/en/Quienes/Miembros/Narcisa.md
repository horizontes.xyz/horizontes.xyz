---
identifier: narcisa
title: Narcisa Bautista Ramos
img: Miembros/Narcisa.jpg
---

Member of the Tikuna Ethnic Group 
Bilingual student of Indigenous Politics Democratic Formation in the “Caminos de Identidad” (Trails of Identity), FUCAI. Indigenous Leader of the Community of Nazareth; of which she has been Vice-curaca, carrying out projects for the childhood, youth and women.
Active member of the Corporación Horizontes Colombianos and ACITAM.
