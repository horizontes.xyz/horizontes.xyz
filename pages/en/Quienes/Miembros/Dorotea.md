---
identifier: doro
title: Dorothea Wolf – Nuernberg
img: Miembros/Doro.jpg
---

She was born on January 9, 1962 in Germany. She has 15 years of experience working as a professional nurse. For several years she worked for and founded different non-profit organizations in her country. For 2 years she performed administrative tasks in the Israeli Embassy in Germany. She lived and worked a couple of years in Ginosar / Galilea / Israel (social volunteering). She has been living in Bogota, Colombia since 1999, where she initially taught private English and German lessons (1999-2000), afterwards she wanted to experiment in the field of audiovisual media (2000 - 2002). In October 2002 she decided to found the non-profit organization Corporación Horizontes Colombianos, whose the Legal Representative and General Director she is. She has 37 years of experience in volunteer management and being a volunteer herself. Languages: German, Spanish, English, Hebrew and French.

She is a person with a high sense of social responsibility and strongly committed with the achievement of results. She is very demanding in terms of personal growth with great abilities to work within a team; as well she has expectations for permanent progress.
