---
identifier: laura
title: Laura Cristina Bahamón Villalba
img: Miembros/Laura.jpg
---

Born on March 9th, 1989 in Neiva-Huila (Colombia). Bachelor in arts education and culture, pursuing specialization in childhood and youth. Linked to the educational work with children and youth of the indigenous Nasa communities in the department of Huila and Cauca, she has as well developed visual and corporal art practices focused on children with special educational needs, with an interest in the development of artistic practices and cultural factors for social transformation. Member and volunteer of the Corporación Horizontes Colombianos since 2018.
