---
date: 2019-01-01
identifier: quienes
layout: layouts/page.njk
permalink: /{{ locale }}/{{ parent | slug }}/{{ title | slug }}.html
title: About us
tags:
 - nav_en
---
<center>

![](/static/Logo_CHC.png)
</center>

The Corporación Horizontes Colombianos - CHC - is a non-governmental organization that works for the development of native peoples, the Afro-descendant and peasant population, convinced of the vital importance that this entails for the conservation of natural, cultural and ethnic heritage.

In order to strengthen the autonomy and inclusion and ensure the survival of the Peoples, we have been providing empowerment tools to the communities, so that they exercise their fundamental rights and become architects of their own development, thus contributing to society with their knowledge, experience and wisdom, which assume an increasingly important role in modern life, nuanced by the abuse of natural resources, environmental disasters and the denaturation of values.
