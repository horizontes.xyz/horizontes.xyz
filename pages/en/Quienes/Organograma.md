---
date: 2019-01-04
identifier: organograma
permalink: /{{ locale }}/{{ parent | slug }}/{{ title | slug}}.html
title: Organization Chart
layout: layouts/chart.njk
---

## General Assembly

<pre class="mermaid">
  graph TD

  A[General Assembly] --- B1[Financial Controller]
  A --- B2[Members' Council]
  B2 --- C1[Legal Representative]
  C1 --- D1[Consulting Group <br/> and Agreements]
  C1 --- D2[Indigenous <br/> Representation]
  D2 --- E1[Representative in <br/> Germany]
  E1 --- F1[Assistant <br/> Manager]
  B2 --- C2[Intercultural <br/> Communicator]
  C2 --- D3[Cultural Manager]
  D3 --- E2[Project consultant]
  E2 --- F2[Environmental Area <br/> Coordinator]
  F2 --- G2[Legal adviser]

  classDef someclass fill:#f96;
  class A someclass;
</pre>

## Consulting Group and Agreements

<pre class="mermaid">
  graph LR
  A[Consulting Group <br/> and Agreements]
  A --- B1[Pontificia Universiad <br/> Javeriana]
  B1 --- C1[Dep. of Civil Engineering]
  B1 --- C2[Accounting Dep.]
  B1 --- C3[Dep. of Business Administration]
  B1 --- C4[Fac. of Environmental and Rural Studies]
  B1 --- C5[Fac. of Technologies]
  B1 --- C6[Fac. of Psychology]  
  B1 --- C7[Dep. of Modern Languages]  
  B1 --- C8[School of Nursing]
  B1 --- C9[Law School]
  A --- B2[Politécnico Grancolombiano]
  B2 --- C10[Fac. of Audiovisual Media]
  B2 --- C11[Law School]
  A --- B3[Other Universities]
  B3 --- C12[Univ. Jorge Tadeo Lozano]
  B3 --- C13[Univ. Nacional de Colombia Amazon Headquarters]
  B3 --- C14[Univ. Pedagógica Bogotá]
  B3 --- C15[Univ. Nacional de Colombia Medellin Headquarters]
  B3 --- C16[Univ. Nacional Palmira Headquarters]
  A --- B4[Volunteer Networks]
  B4 --- C17[Alianza Social Uniandinos]
  B4 --- C18[ONU Voluntariado]
  B4 --- C19[FONDACIO]
  A --- B5[Horizontes e. V- Alemania]
  A --- B6[ACITAM]
  B6 --- C20[Asociación de Cabildos Indigenas del Trapecio Amazónico]
  A --- B7[ONIC]
  B7 --- C21[Organización Nacional Indigena de Colombia]
  A --- B8[Other Alliances]
  B8 --- C22[Parroquia Maximilian Kolbe]
  B8 --- C23[EVT - Alemania]
  B8 --- C24[Tu Patrocinnio]
  B8 --- C25[Aid Connect]
  B8 --- C26[We care Charity Label]
  B8 --- C27[Parroquia Frohnleichnam]

  classDef someclass fill:#f96;
  class A someclass;
  class B1 someclass;
  class B2 someclass;
  class B3 someclass;
  class B4 someclass;
  class B5 someclass;
  class B6 someclass;
  class B7 someclass;
  class B8 someclass;
</pre>
