---
date: 2019-01-03
identifier: objetivos
permalink: /{{ locale }}/{{ parent | slug }}/{{ title | slug }}.html
title: Objectives
img: objectives.jpg
---

Our work is in alignment with the Objectives of The Millennium; among others, to eradicate poverty, achieve basic education coverage, reduce infant mortality, and promote gender equality and women’s autonomy, respecting the culture and traditions of the native people.

Through our projects we seek to:

 - Create more and better opportunities for the population.
 - Promote the access to higher education, regardless of gender.
 - Create pacific defense mechanisms for the communities that are in the middle of the armed conflict.
 - Create higher autonomy and inclusion.
 - Reinforce native pride.
 - Improve the living conditions for the children in the tribes.
 - Create an atmosphere of positivism and motivation.
 - Increase income and quality of life.
 - Strengthen and reinforce the native traditions, values and identity.
 - Improve the hygiene and health conditions of the population.
 - Create healthier and more productive environments.
 - Improve the conditions and stability of the community.
 - Generating new and higher leveled knowledge, taking advantage of the new technologies.
