---
date: 2019-01-05
identifier: estructura
permalink: /{{ locale }}/{{ parent | slug }}/{{ title | slug}}.html
title: Statutes
---
**STATUES CORPORACIÓN HORIZONTES COLOMBIANOS**

**CHAPTER I NAME, ADDRESS, SOCIAL OBJECT, DURATION.**

**Art 1 ENTITY NAME.** The entity that by means of these statues, is legalized, is a nonprofit organization, that constitutes as Corporation, and named as CORPORACION HORIZONTES COLOMBIANOS, can acting as well, under CHC

**Art 2 ADDRESS.** The entity that is constitutes, has its headquarters in Bogotá D.C. – Colombia, South America, at CR 6 B Este No 88 G – 18 Sur, Phone 310 254 5321 – 312 604 9037, and capable to open offices in other cities and other countries.

**Art 3 DURATION.** The entity that is constituted, has duration of sixty years (60) years, starting on October 17th, 2002.

**Art 4 SOCIAL OBJECT.** Obtain the financial and institutional support of natural and legal persons focused on improving conditions of education, health, social welfare, as well as the visibility, promotion and dissemination of the practices, knowledge and traditions of indigenous, Afro-descendant, peasant communities and their own diverse manifestations.

Promote initiatives, programs or projects of holistic traditional medicine, with an inter-cultural approach, environmental education, community research, research-creation, alternative tourism, coexistence and education for peace, accompanied by activities that seek to improve the quality of life and coexistence.

Promote actions for peace, coexistence, environment, traditional knowledge and human rights, as well as the promotion of art and ritual as social awareness strategies among communities with an intercultural and experiential approach.

Facilitate cultural exchange with people from different countries who express interest in sharing with ancestral and rural communities in Colombia and Latin America.

[download full document as pdf](/static/estatutos_pdf_en_ak.pdf)
