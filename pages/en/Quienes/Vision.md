---
date: 2019-01-02
identifier: vision
permalink: /{{ locale }}/{{ parent | slug }}/{{ navtitle | slug }}.html
navtitle: Vision and Mission
---
<div class="flex wrap justify-center">
<div class="item pa-r">

![](/static/mission.jpg)
# Mission
To enable the dignified life and autonomy of the original, Afro-descendant and peasant communities through the strengthening of education and the promotion of projects, where the CHC is to serve as a bridge. Likewise, the search for financial resources with the aim of achieving economic, ecological and cultural solutions for those projects that have priority for the communities.
</div>
<div class="item">

![](/static/vision.jpg)
# Vision
We will develop social and productive projects that allow us to achieve autonomy and improve the quality of life of ethnic and rural minorities through experiential encounters, research, practice and artistic creation, as well as environmental, cultural, traditional and cultural initiatives, rituals that enable the transformation of imaginary and social relationships.
</div>
</div>
