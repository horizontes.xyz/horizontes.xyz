function closeDropdown() {
  var dropdowns = document.getElementsByClassName("dropdown-content");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
    var openDropdown = dropdowns[i];
    if (openDropdown.classList.contains('show')) {
      openDropdown.classList.remove('show');
    }
  }
}
function openDropdown(id) {
  var el = document.getElementById(id)
  var toggle = true
  if (el.classList.contains('show')) {
    toggle = false
  }
  closeDropdown()
  if (toggle) {
    el.classList.toggle("show");
  }
}
